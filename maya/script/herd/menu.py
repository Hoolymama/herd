import pymel.core as pm

import os
import glob
 

MAYA_PARENT_WINDOW = "MayaWindow"
HOOLY_MENU = "HoolyMenu"

def ensure_hooly_menu():
    menu = next((m for m in pm.lsUI(menus=True) if m.getLabel() == "Hooly"), None)
    if menu:
        return menu
    return pm.menu(HOOLY_MENU, label="Hooly", tearOff=True, parent=MAYA_PARENT_WINDOW)
 

class HerdMenu(object):
    def __init__(self):
        self.hooly_menu = ensure_hooly_menu()
        pm.setParent(self.hooly_menu, menu=True)
        
        pm.menuItem(label="Herd", subMenu=True)

        pm.menuItem(
        label="Create PP Emitter",
        command=pm.Callback(create_pp_emitter)
    )

def create_pp_emitter():
    pm.mel.source("createPPEmitter")
    pm.mel.eval( 'createPPEmitter(0)' )



