#ifndef _meshVolumeEmitter
#define _meshVolumeEmitter

#include <maya/MIOStream.h>
#include <maya/MTime.h>
#include <maya/MVector.h>
#include <maya/MObject.h>

#include <maya/MBoundingBox.h>

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MPxEmitterNode.h>

#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include "errorMacros.h"


class meshVolumeEmitter: public MPxEmitterNode
{
public:
	meshVolumeEmitter();
	virtual ~meshVolumeEmitter();

	
	
	static void		*creator();
	static MStatus	initialize();
	virtual MStatus	compute( const MPlug& plug, MDataBlock& block );
	
	
	static MObject aInMesh;
	
	static MObject aDivisions;

	
	// geometry samples output to field
	static MObject aSamplePoints;
	static MObject aSampleVelocities;
	static MObject aSampleMasses;
	static MObject aSampleDeltaTime;
	static MObject aSampleFieldData;
	// forces returned
	static MObject aForces;



	static MObject aForceRate;
	static MObject aInheritForce;


	static MTypeId	id;


private:
	
	bool	isFullValue( int plugIndex, MDataBlock& block );
	long	seedValue( int plugIndex, MDataBlock& block );
	double	inheritFactorValue( int plugIndex, MDataBlock& block );

	MStatus  gridPointsInMesh(
		const MObject &inMesh, 
		const double divs, 
		MFloatPointArray &pa,
		float & spacing,
		MBoundingBox &box);
		
	bool sampleTexRate( MFloatPointArray &pa, MFloatArray &densities) ;

	MStatus getAppliedForces(
		 MDataBlock& block,
		 const MVectorArray &positions,
		 const MVectorArray &velocities,
		 const MDoubleArray &masses,
		 const MTime &dT,
		 MVectorArray &appliedForce
		);
		
		MTime	startTimeValue( int plugIndex, MDataBlock& block );
		MTime	deltaTimeValue( int plugIndex, MDataBlock& block );
};



inline bool meshVolumeEmitter::isFullValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	bool value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( mIsFull, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asBool();
		}
	}

	return( value );
}
inline long meshVolumeEmitter::seedValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	long value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( mSeed, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asInt();
				//cerr << value << endl;
		}
	}

	return( value );
}

inline double meshVolumeEmitter::inheritFactorValue(int plugIndex,MDataBlock& block)
{
	MStatus status;
	double value = 0.0;

	MArrayDataHandle mhValue = block.inputArrayValue( mInheritFactor, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asDouble();
		}
	}

	return( value );
}


inline MTime meshVolumeEmitter::startTimeValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = block.inputArrayValue( mStartTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}

	return( value );
}

inline MTime meshVolumeEmitter::deltaTimeValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = block.inputArrayValue( mDeltaTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}
	return( value );
}



#endif
