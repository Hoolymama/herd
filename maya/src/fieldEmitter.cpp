

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MDataHandle.h>
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MPlugArray.h>
#include <maya/MRenderUtil.h> 
#include <maya/MQuaternion.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MArrayDataBuilder.h>

#include "mayaFX.h"
#include "mayaMath.h"
#include "attrUtils.h"
#include "jMayaIds.h"
#include "fieldEmitter.h"

const double oneThird = 0.3333333333333333333333333;

MObject fieldEmitter::aSamplePoints;
MObject fieldEmitter::aSampleVelocities;
MObject fieldEmitter::aSampleMasses;
MObject fieldEmitter::aSampleDeltaTime;
MObject fieldEmitter::aSampleFieldData;

MObject fieldEmitter::aForces;

MObject fieldEmitter::aEmitFromCenter;


MObject fieldEmitter::aForceRate;
MObject fieldEmitter::aVelocityRate;
MObject fieldEmitter::aAccelerationRate;

MObject fieldEmitter::aForceRateRemap;
MObject fieldEmitter::aVelocityRateRemap;
MObject fieldEmitter::aAccelerationRateRemap;

MObject fieldEmitter::aForceFacingCurve;
MObject fieldEmitter::aVelocityFacingCurve;
MObject fieldEmitter::aAccelerationFacingCurve;

MObject fieldEmitter::aComponentCalculation;
	

MObject fieldEmitter::aInheritForce;
MObject fieldEmitter::aInheritVelocity;
MObject fieldEmitter::aInheritAcceleration;
MObject fieldEmitter::aSweepTimeOffset;
MObject fieldEmitter::aSweepTimeFactor;



MTypeId fieldEmitter::id( k_fieldEmitter );


fieldEmitter::fieldEmitter():m_lastTriangleCount(0)
{
}


fieldEmitter::~fieldEmitter()
{
}


void *fieldEmitter::creator()
{
	return new fieldEmitter;
}

void  fieldEmitter::distributeOverTime(
	unsigned tri,
	const double &newRate,
	const double &oldRate,
	const MVector &newP1,
	const MVector &newP2,
	const MVector &newP3,
	const MVector &oldP1,
	const MVector &oldP2,
	const MVector &oldP3,
	const MVector & newVec,
	const MVector & oldVec,
	const double &speedRandom,	
	const double &spread,	
	const double &dt,	
	const double &sweepTimeOffset,
	const double &sweepTimeFactor,
	MVectorArray &pos,
	MVectorArray &vel,
	MDoubleArray &tim,
	MDoubleArray &triId
) {

	const double  PI  = 3.1415927;
	const double  _2PI = 2.0 * PI;
	// cerr << "N: " << newVec << " -- O: " << oldVec ;
	// cerr << "  ---  Diff:" << (newVec - oldVec).length() << endl;

	// stuff needed for spread 
	//////////////////////////////////

	double rot_min = cos(spread*PI);
	double rot_t, rot_w, rot_z;
	double rot_range = 1.0 - rot_min;
	//////////////////////////////////
	

	double s, t, oneMinusSweep;
	MVector sweepVertex1, sweepVertex2, sweepVertex3;


	double highRate = (newRate > oldRate) ? newRate : oldRate;
	// double deltaRate = newRate - oldRate;
	MVector deltaVec(newVec - oldVec);

	//double newRateRecip = newRate / 1.0;
	unsigned int nHighRate = mayaMath::randCount(highRate);
	
	
	for(unsigned i = 0; i < nHighRate; ++i)
	{
		
		double sweep = drand48() *  sweepTimeFactor;		
		//	double sweep = ( double(i) + drand48() ) / double(nAvgRate); // more even spread
		
		
		// double v = (oldRate + (deltaRate*sweep)) * newRateRecip;
		
		// bool doParticle =  (drand48() < v);
		bool doParticle =  fmod(newRate, 1.0) > drand48();

		if (doParticle) {

			// find position at which to emit particle
			s = drand48();
			t = drand48();
			if ((s + t) > 1) {
				s = 1.0 - s;
				t = 1.0 - t;
			}
			// sweep = drand48();
			oneMinusSweep = 1.0 - sweep;
			double w = (1-(s+t));

			sweepVertex1 = ((newP1*sweep) + (oldP1*oneMinusSweep));
			sweepVertex2 = ((newP2*sweep) + (oldP2*oneMinusSweep));
			sweepVertex3 = ((newP3*sweep) + (oldP3*oneMinusSweep));

			MVector vec = oldVec + (deltaVec * sweep );
			MQuaternion q(MVector::zAxis, vec);
			MVector rot_v;

			
	    	double rot_vl = vec.length() *  (1.0 + (speedRandom * 2.0 * (drand48() -0.5)));
	
	    	rot_z = (drand48() * rot_range) + rot_min;
	    	rot_t = drand48() * _2PI;
	    	rot_w = (sqrt( 1.0 - rot_z*rot_z )) * rot_vl;
	    	rot_v.z = rot_z * rot_vl;
	    	rot_v.x = rot_w * cos( rot_t );
	    	rot_v.y = rot_w * sin( rot_t );
	    	rot_v = rot_v.rotateBy(q);
			
			//double rot_vl = vec.length() *  (1.0 + (speedRandom * 2.0 * (drand48() -0.5)));
			//rot_v = vec * (1.0 + (speedRandom * 2.0 * (drand48() -0.5)));
			
			
			//rot_v  = vec;
			vel.append(rot_v);

			// particles that were emitted at the beginning of the frame will have travelled a bit by now
			MVector posOffset = (rot_v * dt * (1.0-(sweep - sweepTimeOffset)) );

			pos.append( (sweepVertex1*w)  + (sweepVertex2*s) +  (sweepVertex3*t)  + posOffset) ;	
			tim.append( sweep);
			triId.append(tri);
		}

		/* code */
	}
}


void  fieldEmitter::distributeOverTime(
	unsigned tri,
	const double &newRate,
	const double &oldRate,
	const MVector &newC,
	const MVector &oldC,
	const MVector & newVec,
	const MVector & oldVec,
	const double &speedRandom,	
	const double &spread,	
	const double &dt,	
	const double &sweepTimeOffset,
	const double &sweepTimeFactor,
	MVectorArray &pos,
	MVectorArray &vel,
	MDoubleArray &tim,
	MDoubleArray &triId
) {

	const double  PI  = 3.1415927;
	const double  _2PI = 2.0 * PI;



	// stuff needed for spread 
	//////////////////////////////////

	double rot_min = cos(spread*PI);
	double rot_t, rot_w, rot_z;
	double rot_range = 1.0 - rot_min;
	//////////////////////////////////
	

	double s, t, oneMinusSweep;
	MVector sweepVertex;


	double highRate = (newRate > oldRate) ? newRate : oldRate;
	// double deltaRate = newRate - oldRate;
	MVector deltaVec(newVec - oldVec);

	//double newRateRecip = newRate / 1.0;
	unsigned int nHighRate = mayaMath::randCount(highRate);
	
	
	for(unsigned i = 0; i < nHighRate; ++i)
	{
		
		double sweep = drand48() *  sweepTimeFactor;		
		//	double sweep = ( double(i) + drand48() ) / double(nAvgRate); // more even spread
		
		
		// double v = (oldRate + (deltaRate*sweep)) * newRateRecip;
		
		// bool doParticle =  (drand48() < v);
		bool doParticle =  fmod(newRate, 1.0) > drand48();

		if (doParticle) {

			// // find position at which to emit particle
			// s = drand48();
			// t = drand48();
			// if ((s + t) > 1) {
			// 	s = 1.0 - s;
			// 	t = 1.0 - t;
			// }
			//sweep = drand48();
			oneMinusSweep = 1.0 - sweep;

			sweepVertex = ((newC*sweep) + (oldC*oneMinusSweep));

			MVector vec = oldVec + (deltaVec * sweep );
			MQuaternion q(MVector::zAxis, vec);
			MVector rot_v;

	    	double rot_vl = vec.length() *  (1.0 + (speedRandom * 2.0 * (drand48() -0.5)));
	
	    	rot_z = (drand48() * rot_range) + rot_min;
	    	rot_t = drand48() * _2PI;
	    	rot_w = (sqrt( 1.0 - rot_z*rot_z )) * rot_vl;
	    	rot_v.z = rot_z * rot_vl;
	    	rot_v.x = rot_w * cos( rot_t );
	    	rot_v.y = rot_w * sin( rot_t );
	    	rot_v = rot_v.rotateBy(q);
			
			//double rot_vl = vec.length() *  (1.0 + (speedRandom * 2.0 * (drand48() -0.5)));
			//rot_v = vec * (1.0 + (speedRandom * 2.0 * (drand48() -0.5)));
			
			
			//rot_v  = vec;
			vel.append(rot_v);

			// particles that were emitted at the beginning of the frame will have travelled a bit by now
			MVector posOffset = (rot_v * dt * (1.0-(sweep - sweepTimeOffset)) );

			pos.append( sweepVertex + posOffset) ;	
			tim.append( sweep);
			triId.append(tri);
		}

		/* code */
	}
}


MStatus fieldEmitter::initialize()

{
	MStatus st;
//  unsigned int counter = 0;
	MString method("fieldEmitter::initialize");

	MFnTypedAttribute tAttr;
	MFnCompoundAttribute cAttr;
	MFnUnitAttribute uAttr;
	MFnEnumAttribute eAttr;
	MFnNumericAttribute nAttr;


	aEmitFromCenter =  nAttr.create( "emitFromCenter", "emc", MFnNumericData::kBoolean );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(false);
	st = addAttribute(aEmitFromCenter);mser;


	aForceRate =  nAttr.create( "forceRate", "fcr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aForceRate);mser;



	aVelocityRate =  nAttr.create( "velocityRate", "vlr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aVelocityRate);mser;

	aAccelerationRate =  nAttr.create( "accelerationRate", "acr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aAccelerationRate);mser;
	;


	aForceRateRemap =  nAttr.create( "forceRateRemap", "fcrr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aForceRateRemap);mser;


	aVelocityRateRemap =  nAttr.create( "velocityRateRemap", "vlrr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aVelocityRateRemap);mser;

	aAccelerationRateRemap =  nAttr.create( "accelerationRateRemap", "acrr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aAccelerationRateRemap);mser;
	;


// 	enum CompCalc { kFpApV, kFmAmV, kFmApV, kAmFpV , kVmFpA, kFpAmV, kApFmV , kVpFmA };
	
	
	aComponentCalculation = eAttr.create("rateCalculation", "rcl");
	eAttr.addField("Frc + Acc + Vel", kFpApV);
	eAttr.addField("Frc * Acc * Vel", kFmAmV);
	eAttr.addField("Frc * (Acc + Vel)", kFmApV);
	eAttr.addField("Acc * (Frc + Vel)", kAmFpV);
	eAttr.addField("Vel * (Frc + Acc)", kVmFpA);
	eAttr.addField("Frc + (Acc * Vel)", kFpAmV);
	eAttr.addField("Acc + (Frc * Vel)", kApFmV);
	eAttr.addField("Vel + (Frc * Acc)", kVpFmA);

	eAttr.setDefault(kFpApV);
	eAttr.setKeyable(true);
	eAttr.setWritable(true);
	st = addAttribute(aComponentCalculation );mser;





	aInheritForce =  nAttr.create( "inheritForce", "ifc", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aInheritForce);	mser;

	aInheritVelocity =  nAttr.create( "inheritVelocity", "ivl", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aInheritVelocity);	mser;

	aInheritAcceleration =  nAttr.create( "inheritAcceleration", "iac", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aInheritAcceleration);	mser;

	aSweepTimeOffset =  nAttr.create( "sweepTimeOffset", "sto", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aSweepTimeOffset);	mser;

	aSweepTimeFactor =  nAttr.create( "sweepTimeFactor", "stf", MFnNumericData::kDouble, 1.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st =	addAttribute(aSweepTimeFactor);	mser;



	aForceFacingCurve	= nAttr.create("forceFacingCurve","ffc", MFnNumericData::kDouble, 1.0, &st); mser;
	nAttr.setHidden(false);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setWritable(true);
	st = addAttribute(aForceFacingCurve); mser;

	aVelocityFacingCurve = nAttr.create("velocityFacingCurve","vfc", MFnNumericData::kDouble, 1.0, &st); mser;
	nAttr.setHidden(false);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setWritable(true);
	st = addAttribute(aVelocityFacingCurve); mser;

	aAccelerationFacingCurve = nAttr.create("accelerationFacingCurve","acfc", MFnNumericData::kDouble, 1.0, &st); mser;
	nAttr.setHidden(false);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setWritable(true);
	st = addAttribute(aAccelerationFacingCurve); mser;

	aSamplePoints = tAttr.create("samplePoints", "spts", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleVelocities = tAttr.create("sampleVelocities", "svls", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleMasses = tAttr.create("sampleMasses", "smss", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleDeltaTime = uAttr.create( "sampleDeltaTime", "sdt", MFnUnitAttribute::kTime, 0.0, &st ); mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleFieldData = cAttr.create("sampleFieldData","sfd");
	cAttr.addChild(aSamplePoints);
	cAttr.addChild(aSampleVelocities);
	cAttr.addChild(aSampleMasses);
	cAttr.addChild(aSampleDeltaTime);

	//st = addAttribute(aSamplePoints );mser;
//st = addAttribute(aSampleVelocities );mser;
//st = addAttribute(aSampleMasses );mser;
	//st = addAttribute(aSampleDeltaTime);mser;
	st = addAttribute(aSampleFieldData);mser;


	aForces = tAttr.create("forces", "frc", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setArray(true);
	st = addAttribute( aForces ); mser;

	attributeAffects(mCurrentTime,aSamplePoints);
	attributeAffects(mCurrentTime,aSampleVelocities);
	attributeAffects(mCurrentTime,aSampleMasses);
	attributeAffects(mCurrentTime,aSampleFieldData);

	return( MS::kSuccess );
}


MStatus fieldEmitter::getLookup(const MObject &node, MObject &attr, lookup & result){
	MStatus st;
	MFnAnimCurve aniFn; 
	st = getAniCurveFn(node, attr, aniFn);
	if (!(st.error())) 	result.create(aniFn, 20);
	return st;
}
	

MStatus fieldEmitter::compute(const MPlug& plug, MDataBlock& block)
{
	

	MStatus st;
	MString method("fieldEmitter::compute");
	if( !(plug == mOutput) ) return( MS::kUnknownParameter );
	
		// has no effect
	if ( block.inputValue( state ).asShort() == 1 ) return MS::kSuccess;
	// cerr << "------------------" << endl;
	
	MObject thisNode = thisMObject();
	MFnDependencyNode thisNodeFn(thisNode);

	MObject  aSpread = thisNodeFn.attribute(MString("spread"));
	MObject  aNormalSpeed = thisNodeFn.attribute(MString("normalSpeed"));
	MObject  aSpeedRandom = thisNodeFn.attribute(MString("speedRandom"));
	MObject  aScaleRate = thisNodeFn.attribute(MString("scaleRateByObjectSize"));
	

	bool emitFromCenter = block.inputValue(aEmitFromCenter).asBool();
	

	int multiIndex = plug.logicalIndex( &st);mser;
	MArrayDataHandle hOutArray = block.outputArrayValue(mOutput, &st);mser;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st);mser;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;
	MFnArrayAttrsData fnOutput;
	MObject dOutput = fnOutput.create ( &st );mser;
	

	// position and velocity arrays to append new particle data.
	MVectorArray outPos = fnOutput.vectorArray("position", &st);mser;
	MVectorArray outVel = fnOutput.vectorArray("velocity", &st);mser;
	MDoubleArray outTim = fnOutput.doubleArray("timeInStep", &st);mser;
	MDoubleArray outTrianglId = fnOutput.doubleArray("triangleId", &st);mser;

	

	// check time and isFull
	bool beenFull = isFullValue( multiIndex, block );
	if( beenFull ) return( MS::kSuccess );
	MTime cT = currentTimeValue( block );
	MTime sT = startTimeValue( multiIndex, block );
	MTime dT = deltaTimeValue( multiIndex, block );
	double dt =  dT.as(MTime::kSeconds);
	double dtRecip = 1.0 / dt;
		

	//cerr << "cT: "<< cT << endl;
	//cerr << "dT: "<< dT << endl;
		

	if( (cT <= sT) || (dT <= 0.0) )
	{
		if(dT < 0.0) {
			m_lastVelocity.clear();
		//	m_lastAcceleration.clear();
		}
		hOut.set( dOutput );
		block.setClean( plug );
		return( MS::kSuccess );
	}
	

	// Get the swept geometry data
	//////////////////////////////////////////////////////////
	MObject thisObj = this->thisMObject();
	MPlug sweptPlug( thisObj, mSweptGeometry );
	

	if (! sweptPlug.isConnected() ) {
		hOut.set( dOutput );
		block.setClean( plug );
		return( MS::kSuccess );
	}
	


	
	MObject  aEnableTextureRate = thisNodeFn.attribute(MString("enableTextureRate"));
	bool useTexRate = block.inputValue(aEnableTextureRate).asBool();
		

	// get triangle data for sampling purposes
	// Note, we have to access the swept geometry again later to
	// get vertex positins and UVs
	//////////////////////////////////////////////////////////
	MDataHandle sweptHandle = block.inputValue( mSweptGeometry );
	MObject sweptData = sweptHandle.data();
	MVectorArray centers, velocities, accelerations, forces;
	MDoubleArray areas;
	double totalArea = 0;
		

	
	MFloatArray uCoords;
	MFloatArray vCoords;
	MFloatPointArray samplePoints;
	MFloatVectorArray sampleNormals;
	
		

	bool triangleCountChanged ;
	if (useTexRate) {
		 triangleCountChanged = mayaFX::getDynTriangleArrays( dtRecip, m_lastVelocity, m_lastTriangleCount,	sweptData, centers, velocities, accelerations, areas,totalArea,uCoords,vCoords,samplePoints,sampleNormals);
	} else {
		 triangleCountChanged = mayaFX::getDynTriangleArrays( dtRecip, m_lastVelocity, m_lastTriangleCount,	sweptData, centers, velocities, accelerations, areas,totalArea);
	}
		

	unsigned numTriangles = centers.length();
	m_lastTriangleCount = numTriangles;
	m_lastVelocity.copy(velocities);
	

	if (! numTriangles) return( MS::kSuccess );
	if (triangleCountChanged) {
		m_lastEmissionRate = 	MDoubleArray(numTriangles);
		m_lastEmissionVec = MVectorArray(numTriangles);
	}
		

	//////////////////////////////////////////////////////////
	
//	cerr << "here 1" << endl;
	// Get ani curve lookups
	//////////////////////////////////////////////////////////
	

	lookup forceRateLut, velocityRateLut, accelerationRateLut;
	st = getLookup(thisNode, aForceFacingCurve,forceRateLut);mser;
	st = getLookup(thisNode, aVelocityFacingCurve,velocityRateLut);mser;
	st = getLookup(thisNode, aAccelerationFacingCurve,accelerationRateLut);mser;
	
	lookup forceRateRemap, velocityRateRemap, accelerationRateRemap;
	st = getLookup(thisNode, aForceRateRemap,forceRateRemap);mser;
	st = getLookup(thisNode, aVelocityRateRemap,velocityRateRemap);mser;
	st = getLookup(thisNode, aAccelerationRateRemap,accelerationRateRemap);mser;
	//////////////////////////////////////////////////////////
	
		

//	cerr << "here 2" << endl;
	
	// Get rate values
	//////////////////////////////////////////////////////////
	double theRate =  block.inputValue( mRate ).asDouble() ;
	if (theRate < 0.0) theRate = 0.0;
	double forceRate =  block.inputValue( aForceRate ).asDouble() ;
	double velocityRate =  block.inputValue( aVelocityRate ).asDouble() ;
	double accelerationRate =  block.inputValue( aAccelerationRate ).asDouble() ;
	CompCalc calc = (CompCalc)block.inputValue(aComponentCalculation).asShort();
	bool scaleRate = block.inputValue(aScaleRate).asBool(); 
	if (!scaleRate) theRate = theRate / totalArea;	
	//////////////////////////////////////////////////////////	
	

//	cerr << "here 3" << endl;

	// is the rate mapped ?
	//////////////////////////////////////////////////////////

	MFloatArray texturedRates;
	if (useTexRate) {
	MObject  aTextureRate = thisNodeFn.attribute(MString("textureRate"));
		useTexRate =  mayaFX::sampleTexture(thisNode,aTextureRate,uCoords,vCoords,samplePoints,sampleNormals,texturedRates);
	}
	//////////////////////////////////////////////////////////
	

//	cerr << "here 4" << endl;

	// are forces blowing on the surface?
	//////////////////////////////////////////////////////////		
	st = mayaFX::collectExternalForces(thisNode, block, centers, velocities, areas, dT, forces);mser;
	//////////////////////////////////////////////////////////	
//	cerr << "here 5" << endl;

	// Get attributes.
	//////////////////////////////////////////////////////////
	long seedVal = seedValue( multiIndex, block );
	double speed = doubleValue( block, mSpeed );
	double speedRandom = doubleValue( block, aSpeedRandom );
	double normalSpeed = doubleValue( block, aNormalSpeed );
	double spread = doubleValue( block, aSpread );
	double inheritForce = doubleValue( block, aInheritForce );
	double inheritVelocity = doubleValue( block, aInheritVelocity );
	double inheritAcceleration = doubleValue( block, aInheritAcceleration );
	double sweepTimeOffset = doubleValue( block, aSweepTimeOffset );
	double sweepTimeFactor = doubleValue( block, aSweepTimeFactor );
	


	if (spread > 1 ) spread = 1;
	if (spread < 0 ) spread = 0;
	MVector dirV = vectorValue( block, mDirection );

	


	srand48(seedVal);
	
	MDoubleArray stashedRateArray(numTriangles);
	MVectorArray stashedVelocityArray(numTriangles);
		

	for (unsigned t=0; t < numTriangles; t++ )
	{
		MFnDynSweptGeometryData fnSweptData( sweptData );
		MDynSweptTriangle tri = fnSweptData.sweptTriangle( t );


		MVector N = tri.normal();
//		cerr << "here 6" << endl;
		float velocityFacingMult = 1.0f;
		double velocityMag = velocities[t].length();
		MVector velocityNorm;
		if (velocityMag > 0.0) {
			velocityNorm = velocities[t] / velocityMag;
			if (!velocityRateLut.isConstantOne()) {
				float dot = float(velocityNorm*N);
				velocityFacingMult = velocityRateLut.evaluate(dot);
			}

		}
	//	cerr << "here 7" << endl;

		float forceFacingMult = 1.0f;
		double forceMag = forces[t].length();
		MVector forceNorm;
		if (forceMag > 0.0) {
			forceNorm = forces[t] / forceMag;
			if (!forceRateLut.isConstantOne()) {
				float dot = float(forceNorm*N);
				forceFacingMult = forceRateLut.evaluate(dot);
				//cerr << "forceFacingMult = " << forceFacingMult  << endl;
			} else {
				//cerr << "is constant one" << endl;
			}
		}
	//	cerr << "here 8" << endl;

		float accelFacingMult = 1.0f;
		double accelMag = accelerations[t].length();
		MVector accelNorm;
		if (accelMag > 0.0) {
			accelNorm = accelerations[t] / accelMag;
			if (!accelerationRateLut.isConstantOne()) {
				float dot = float(accelNorm*velocityNorm);
				accelFacingMult = accelerationRateLut.evaluate(dot);
			}
		}
		//cerr << "here 9" << endl;
	

		if (!forceRateRemap.isConstantOne()) forceMag = forceRateRemap.evaluate(float(forceMag));
		if (!velocityRateRemap.isConstantOne()) velocityMag = velocityRateRemap.evaluate(float(velocityMag));
		if (!accelerationRateRemap.isConstantOne()) accelMag = accelerationRateRemap.evaluate(float(accelMag));

		//cerr << "here 10" << endl;

		// calculate this rate.
		
			

		double thisRate ;
		double fr = (forceRate * forceMag * forceFacingMult) ; 
		double vr = (velocityRate * velocityMag * velocityFacingMult) ; 
		double ar = (accelerationRate * accelMag * accelFacingMult);
			
					

		switch ( calc ) {
			case fieldEmitter::kFpApV:
			thisRate = fr + ar + vr; 
			break;
			case fieldEmitter::kFmAmV:
			thisRate = fr * ar * vr; 
			break;
			case fieldEmitter::kFmApV:
			thisRate = fr * (ar + vr); 
			break;
			case fieldEmitter::kAmFpV:
			thisRate =  ar * (fr + vr); 
			break;
			case fieldEmitter::kVmFpA:
			thisRate =  vr * (fr + ar); 
			break;
			case fieldEmitter::kFpAmV:
			thisRate = fr + (ar * vr); 
			break;
			case fieldEmitter::kApFmV:
			thisRate = ar + (fr * vr); 
			break;
			case fieldEmitter::kVpFmA:
			thisRate = vr + (fr * ar); 
			break;			
			
			default:
			thisRate = fr + ar + vr; 
			break;
		}			
	

	//	((sides == fieldEmiter::kSidesBoth)						
		//thisRate = thisRate *( velocityFacingMult + forceFacingMult + accelFacingMult) ;
		if (emitFromCenter) {
			thisRate = thisRate * theRate * dt; // mult by dt so that oversampling works
		} else {
			thisRate = thisRate * areas[t] * theRate * dt; // mult by dt so that oversampling works
		}
		
	//	cerr << dt << endl;
		if (useTexRate) thisRate = thisRate * double(texturedRates[t]);
		//const double doubleCount = thisRate;

		if (thisRate < 0.0) thisRate = 0.0;


	

	//	const unsigned int newRate = randCount(thisRate);
		// remember this rate
		stashedRateArray.set(thisRate,t) ;



		// velocities for this tri
		MVector inheritVec(MVector::zero);
		MVector normalVec(MVector::zero);

	

		if (inheritVelocity) inheritVec = (inheritVelocity * velocities[t] ) ;
		if (inheritForce) inheritVec = inheritVec + (inheritForce * forces[t] ) ;
		if (inheritAcceleration) inheritVec = inheritVec + (inheritAcceleration * accelerations[t] ) ;
	

		MVector newVec(MVector::zero);
		MVector dirVec(dirV);
		if (normalSpeed) normalVec = (normalSpeed * N);
		newVec = (dirVec + normalVec + inheritVec ) * speed;

		stashedVelocityArray.set(newVec,t) ;
	

		MVectorArray v0(3);
		MVectorArray v1(3);
		v0[0] = tri.vertex( 0 ,0.0);
		v0[1] = tri.vertex( 1 ,0.0);
		v0[2] = tri.vertex( 2 ,0.0);
		v1[0] = tri.vertex( 0 ,1.0);
		v1[1] = tri.vertex( 1 ,1.0);
		v1[2] = tri.vertex( 2 ,1.0);

		if (emitFromCenter) {

			MVector c1 = (v1[0] + v1[1] + v1[2]) / 3.0;
			MVector c0 = (v0[0] + v0[1] + v0[2]) / 3.0;

			distributeOverTime(	
				t,
				thisRate,
				m_lastEmissionRate[t], 
				c1,c0,
				newVec,
				m_lastEmissionVec[t], 
				speedRandom,
				spread ,
				dt,
				sweepTimeOffset, 
				sweepTimeFactor, 
				outPos,
				outVel,
				outTim,
				outTrianglId);
		} else {

			// create the new positions and velocities interpolated over time
			distributeOverTime(	
				t,
				thisRate,
				m_lastEmissionRate[t], 
				v1[0],v1[1],v1[2],v0[0],v0[1],v0[2],
				newVec,
				m_lastEmissionVec[t], 
				speedRandom,
				spread ,
				dt,
				sweepTimeOffset, 
				sweepTimeFactor, 
				outPos,
				outVel,
				outTim,
				outTrianglId);
		}
	


	//	cerr  << "m_lastEmissionRate[t]: " << m_lastEmissionRate[t] << endl ;
	//	cerr  << "thisRate: " << thisRate << endl ;


	}
		

	m_lastEmissionRate = stashedRateArray;
	m_lastEmissionVec = stashedVelocityArray;

	hOut.set( dOutput );
	block.setClean( plug );
	return( MS::kSuccess );
}


