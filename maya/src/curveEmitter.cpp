

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MDataHandle.h>
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MPlugArray.h>
#include <maya/MRenderUtil.h> 
#include <maya/MQuaternion.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MArrayDataBuilder.h>


#include "mayaFX.h"
#include "mayaMath.h"
#include "attrUtils.h"
#include "jMayaIds.h"
#include "curveEmitter.h"


MObject curveEmitter::aPoints0;
MObject curveEmitter::aPoints1;
MObject curveEmitter::aUValues;
MObject curveEmitter::aUserVectorPP;

MObject curveEmitter::aRadius;
MObject curveEmitter::aRadiusRamp;

MObject curveEmitter::aNormalSpeedRamp;

MObject curveEmitter::aTangentSpeedRamp;

MObject curveEmitter::aOrbitalSpeed;
MObject curveEmitter::aOrbitalSpeedRamp;

MObject curveEmitter::aInheritVelocity;
MObject curveEmitter::aInheritVelocityRamp;

MObject curveEmitter::aVectorSpeed;
MObject curveEmitter::aVectorSpeedRamp;

// MObject curveEmitter::aSweepTimeOffset;
const double  PI  = 3.1415927;
const double  TAU = 2.0 * PI;


MTypeId curveEmitter::id( k_curveEmitter );


curveEmitter::curveEmitter()
{
}

curveEmitter::~curveEmitter()
{
}

void *curveEmitter::creator()
{
	return new curveEmitter;
}


unsigned curveEmitter::emitSegment(
	double rateA, 
	double rateB,
	const MVector &points0A,
	const MVector &points0B,
	const MVector &points1A,
	const MVector &points1B,
	const MVector &userVectorA,
	const MVector &userVectorB,
	float radiusA,
	float radiusB,
	float normalSpeedA,
	float normalSpeedB,
	float tangentSpeedA,
	float tangentSpeedB,
	float orbitalSpeedA ,
	float orbitalSpeedB,
	float inheritedVelocityA,
	float inheritedVelocityB,
	float vectorSpeedA ,
	float vectorSpeedB,
	double speed,
	double speedRandom,
	double spread,
	const MVector &dirV,
	MVectorArray &outPos,
	MVectorArray &outVel,
	MDoubleArray &outTime
) 
{

	// stuff needed for spread 
	//////////////////////////////////
	double rot_min = cos(spread*PI);
	double rot_t, rot_w, rot_z;
	double rot_range = 1.0 - rot_min;
	//////////////////////////////////
	// cerr << "rateA = " << rateA << endl; 
	// cerr << "rateB = " << rateB << endl; 

	double rate = (rateA + rateB) * 0.5 * (points1B - points1A).length();
	if (rate <= 0.0) return 0;
	// cerr << "rate = " << rate << endl; 
	unsigned int nRate = mayaMath::randCount(rate);
		
	if (nRate <= 0) return 0;

	MVector sweepPointA, sweepPointB, sweepPoint, radPoint;
	MVector emissionVec;
	float radius;

	// MVector line = (points1B - points1A);
	// MVector tangent  =  line.normal();
	// find rotation of Z axis to tangent vector
	// cerr << "nRate = " << nRate << endl; 
	for(unsigned i = 0; i < nRate; ++i)
	{
		double sweep = drand48();	// time	
		double param = drand48();	
		float fparam = float(param);	
		double oneMinusParam = 1.0 - param;
		// first, figure out where this particle will be emitted 
		sweepPointA = ((points1A * sweep) +  (points0A * (1.0 - sweep)));
		sweepPointB = ((points1B * sweep) +  (points0B * (1.0 - sweep)));

		MVector changeAtParam = ((points1B * param) +  (points1A * oneMinusParam)) -   ((points0B * param) +  (points0A * oneMinusParam));
		//sweepPoint =  ((sweepPointB * param) +  (sweepPointA * (1.0 - param)));
		
		MVector userVector = (userVectorB * param) + (userVectorA * oneMinusParam);

		// we will do all calcs in space where the line points along the positive Z axis
		// this will be easier on the old brain thing. Then rotate back after
		MVector line = (sweepPointB - sweepPointA);
		double z = param * line.length();;

		MQuaternion q(MVector::zAxis, line);
		double rn1 = drand48();
		double rn2 = drand48();	
		if (rn2 < rn1) {
			double tmp = rn1;
			rn1 = rn2;
			rn2 = tmp;
		}
		radius = ((radiusB * param) +  (radiusA * (1.0 - param)));
		double rnRadius = rn2 * radius;
		double ratio = TAU*rn1/rn2;
		double x = rnRadius * cos(ratio);
		double y = rnRadius * sin(ratio);
		MVector radPoint(x,y,z); 
		radPoint = sweepPointA + radPoint.rotateBy(q);
		// now we have the point of emission, albeit without taking emission
		// time into account. We can do that after we work out velocity. 

		// As we are in local Z space, the normal velocity is a function
		// of the emission point
		float normalSpeed = ((normalSpeedB * fparam) +  (normalSpeedA * (1.0 - fparam)));
		MVector xyNorm(MVector(x,y,0).normal());
		MVector emissionVec = xyNorm * normalSpeed;

		// and tangent is just the z axis
		float tangentSpeed = ((tangentSpeedB * fparam) +  (tangentSpeedA * (1.0 - fparam)));
		emissionVec += MVector::zAxis * tangentSpeed;

		// and orbital is their cross product
		float orbitalSpeed = ((orbitalSpeedB * fparam) +  (orbitalSpeedA * (1.0 - fparam)));
		emissionVec += (xyNorm^MVector::zAxis) * orbitalSpeed;

		// we need to rotate dirV () round to zaxis space to add it in
		emissionVec += dirV.rotateBy(q.inverse());

		// same goes for inherited velocity
		MVector change = ((points1B * param) +  (points1A * (1.0 - param))) -   ((points0B * param) +  (points0A * (1.0 - param)));
		float inheritedVelocity = ((inheritedVelocityB * fparam) +  (inheritedVelocityA * (1.0 - fparam)));
		emissionVec += change.rotateBy(q.inverse()) * inheritedVelocity;

		// now the user vector - again in local space
		float vectorSpeed = ((vectorSpeedB * fparam) +  (vectorSpeedA * (1.0 - fparam)));
		emissionVec += userVector.rotateBy(q.inverse()) * vectorSpeed;

		// now our emission vec needs to be aligned with z axis so we can apply spread "easily"
		MQuaternion qr(MVector::zAxis, emissionVec);
		MVector rot_v;
    	rot_z = (drand48() * rot_range) + rot_min;
    	rot_t = drand48() * TAU;
    	rot_w = sqrt( 1.0 - rot_z*rot_z );
    	rot_v.z = rot_z;
    	rot_v.x = rot_w * cos( rot_t );
    	rot_v.y = rot_w * sin( rot_t );
    	rot_v = rot_v.rotateBy(qr);

    	// finally we can rotate the emission vector back into world space
    	rot_v = rot_v.rotateBy(q);

		// now that we have accumulated all speed components and spread them,
		// we randomize speed
		double randSpeed = speed + (speedRandom * ((drand48() * 2.0)-1.0));
    	emissionVec =  rot_v * emissionVec.length() * randSpeed;

    	outVel.append(emissionVec);
		outPos.append( radPoint + ((1.0 - sweep) * (emissionVec * 0.04))) ;	
		outTime.append( sweep);

	}

	return nRate;
}

MStatus curveEmitter::initialize()

{
	MStatus st;
//  unsigned int counter = 0;
	MString method("curveEmitter::initialize");

	MFnTypedAttribute tAttr;
	MFnCompoundAttribute cAttr;
	MFnUnitAttribute uAttr;
	MFnEnumAttribute eAttr;
	MFnNumericAttribute nAttr;


	aPoints0 = tAttr.create("positions0", "pos0", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aPoints0);	mser;

	aPoints1 = tAttr.create("positions1", "pos1", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aPoints1);	mser;

	aUValues = tAttr.create("uValues", "us", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aUValues);	mser;

	aRadius =  nAttr.create( "radius", "rad", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aRadius);	mser;

	aRadiusRamp = MRampAttribute::createCurveRamp("radiusRamp","rrmp");
	st = addAttribute( aRadiusRamp );mser;

	aNormalSpeedRamp = MRampAttribute::createCurveRamp("normalSpeedRamp","nsrmp");
	st = addAttribute( aNormalSpeedRamp );mser;

	aTangentSpeedRamp = MRampAttribute::createCurveRamp("tangentSpeedRamp","tsrmp");
	st = addAttribute( aTangentSpeedRamp );mser;

	aOrbitalSpeed =  nAttr.create( "orbitalSpeed", "osp", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aOrbitalSpeed);	mser;

	aOrbitalSpeedRamp = MRampAttribute::createCurveRamp("orbitalSpeedRamp","osrmp");
	st = addAttribute( aOrbitalSpeedRamp );mser;

	aInheritVelocity =  nAttr.create( "inheritVelocity", "ivel", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aInheritVelocity);	mser;

	aInheritVelocityRamp = MRampAttribute::createCurveRamp("inheritVelocityRamp","ivrmp");
	st = addAttribute( aInheritVelocityRamp );mser;


	aVectorSpeed =  nAttr.create( "vectorSpeed", "vsp", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aVectorSpeed);	mser;

	aVectorSpeedRamp = MRampAttribute::createCurveRamp("vectorSpeedRamp","vsrmp");
	st = addAttribute( aVectorSpeedRamp );mser;

	aUserVectorPP = tAttr.create("userVectorPP", "vcpp", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aUserVectorPP);	mser;


	return( MS::kSuccess );
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
MStatus curveEmitter::doRampLookup( 
	const MObject& attribute, 
	const MDoubleArray& in, 
	float mult, 
	MFloatArray& results ) const
{
	MStatus st;
	MRampAttribute rampAttr( thisMObject(), attribute, &st ); msert;
	MIntArray ids;
	MFloatArray positions;
	MFloatArray values;
	MIntArray interps;
	// if the ramp is full of 1s then output the mult in each element.
	rampAttr.getEntries(ids,positions,values,interps);
	bool isConstantOne = true;
	for (int i = 0; i < values.length(); ++i)
	{
		if (values[i] != 1.0) {
			isConstantOne = false;
			break;
		}
	}
	unsigned n = in.length();
	results.setLength(n);

	if (isConstantOne) {
		for( unsigned i = 0; i < n; i++ ) {
			results[i] = mult;
		}
	} else {
		for( unsigned i = 0; i < n; i++ )
		{
			float & value = results[i];
			rampAttr.getValueAtPosition( float(in[i]), value, &st ); mser;
			value *= mult;
		}
	}
	return MS::kSuccess;
}



MStatus curveEmitter::compute(const MPlug& plug, MDataBlock& data)
{
	

	MStatus st;
	if( !(plug == mOutput) ) return( MS::kUnknownParameter );
	
		// has no effect
	if ( data.inputValue( state ).asShort() == 1 ) return MS::kSuccess;


	MObject thisNode = thisMObject();
	MFnDependencyNode thisNodeFn(thisNode);
	// JPMDBG;
	int multiIndex = plug.logicalIndex( &st);mser;
	MArrayDataHandle hOutArray = data.outputArrayValue(mOutput, &st);mser;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st);mser;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;
	MFnArrayAttrsData fnOutput;
	MObject dOutput = fnOutput.create ( &st );mser;
	// JPMDBG;

	// position and velocity arrays to append new particle data.
	MVectorArray outPos = fnOutput.vectorArray("position", &st);mser;
	MVectorArray outVel = fnOutput.vectorArray("velocity", &st);mser;
	MDoubleArray outTime = fnOutput.doubleArray("timeInStep", &st);mser;
	// JPMDBG;

	// check time and isFull
	bool beenFull = isFullValue( multiIndex, data );
	if( beenFull ) return( MS::kSuccess );
	MTime cT = currentTimeValue( data );
	MTime sT = startTimeValue( multiIndex, data );
	MTime dT = deltaTimeValue( multiIndex, data );
	double dt =  dT.as(MTime::kSeconds);
	double dtRecip = 1.0 / dt;
	// JPMDBG;

	if( (cT <= sT) || (dT <= 0.0) ){
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}
	// JPMDBG;

	bool validInput = true;
	MDataHandle h;
	MObject d;
	MFnVectorArrayData fnV;
	MFnDoubleArrayData fnD;
	// JPMDBG;

	h = data.inputValue( aPoints1 , &st); msert;
	d = h.data();
	st = fnV.setObject(d); msert;
	MVectorArray points1 = fnV.array( &st );msert;	
	unsigned pl = points1.length();
	if (!pl) validInput = false;
	// JPMDBG;

	h = data.inputValue( aPoints0, &st); msert;
	d = h.data();
	st = fnV.setObject(d);msert;
	MVectorArray points0 = fnV.array( &st );msert;
	if (pl != points0.length()) validInput = false;
	// JPMDBG;

	h = data.inputValue( aUValues , &st); msert;
	d = h.data();
	st = fnD.setObject(d); msert;
	MDoubleArray uValues = fnD.array( &st ); msert;
	if (pl != uValues.length()) validInput = false;
	// JPMDBG;

	double rate =  data.inputValue( mRate ).asDouble() ;
	if (rate <= 0.0) validInput = false;
	// JPMDBG;

	if (! validInput ) {
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}
	// JPMDBG;

	bool hasRatePP = true;
	MObject  aRatePP = thisNodeFn.attribute(MString("ratePP"));	
	h = data.inputValue( aRatePP );
	d = h.data();
	st = fnD.setObject(d);
	if (st.error()) hasRatePP = false;

	MDoubleArray ratePP ;
	if (hasRatePP) {
			// 	cerr << "fnD.length() " << fnD.length() << endl;
			// cerr << "pl " << pl << endl;

		if (fnD.length() == pl) {
			ratePP.copy(fnD.array()) ;
			for (int i = 0; i < pl; ++i)
			{
				ratePP[i] = ratePP[i] * rate;
			}
		} else {
			hasRatePP = false;
		}
	}	

	if (! hasRatePP) {
		// cerr << "No Rate PP" << endl;
		ratePP = MDoubleArray(pl , rate);
	}

	bool hasUserVectorPP = true;
	h = data.inputValue( aUserVectorPP );
	d = h.data();
	st = fnV.setObject(d);
	if (st.error()) hasUserVectorPP = false;

	MVectorArray userVectorPP ;
	if (hasUserVectorPP) {
		if (fnV.length() == pl) {
			userVectorPP.copy(fnV.array()) ;
		} else {
			hasUserVectorPP = false;
		}
	}	

	if (! hasUserVectorPP) {
		userVectorPP = MVectorArray(pl);
	}


	// scalars
	MObject  aSpread = thisNodeFn.attribute(MString("spread"));
	MObject  aNormalSpeed = thisNodeFn.attribute(MString("normalSpeed"));
	MObject  aTangentSpeed = thisNodeFn.attribute(MString("tangentSpeed"));
	MObject  aSpeedRandom = thisNodeFn.attribute(MString("speedRandom"));
	MObject  aScaleRate = thisNodeFn.attribute(MString("scaleRateByObjectSize"));
	// JPMDBG;

	long seedVal = seedValue( multiIndex, data );
	double speed = doubleValue( data, mSpeed );
	double speedRandom = doubleValue( data, aSpeedRandom );
	double normalSpeed = doubleValue( data, aNormalSpeed );
	double spread = doubleValue( data, aSpread );
	double radius = doubleValue( data, aRadius );
	double tangentSpeed = doubleValue( data, aTangentSpeed );
	double orbitalSpeed = doubleValue( data, aOrbitalSpeed );
	double inheritVelocity = doubleValue( data, aInheritVelocity );
	double vectorSpeed = doubleValue( data, aVectorSpeed );
	MVector dirV = vectorValue( data, mDirection );
	if (spread > 1 ) spread = 1;
	if (spread < 0 ) spread = 0;

	// JPMDBG;



	// work out emissionVector at current time for all points
	MFloatArray radii, normalSpeeds, tangentSpeeds, orbitalSpeeds, inheritedVelocities, vectorSpeeds;

	// JPMDBG;


	doRampLookup( curveEmitter::aRadiusRamp, uValues, radius, radii);
	doRampLookup( curveEmitter::aNormalSpeedRamp, uValues, normalSpeed, normalSpeeds);
	doRampLookup( curveEmitter::aTangentSpeedRamp, uValues, tangentSpeed, tangentSpeeds);
	doRampLookup( curveEmitter::aOrbitalSpeedRamp, uValues, orbitalSpeed, orbitalSpeeds);
	doRampLookup( curveEmitter::aInheritVelocityRamp, uValues, inheritVelocity, inheritedVelocities);
	doRampLookup( curveEmitter::aVectorSpeedRamp, uValues, vectorSpeed, vectorSpeeds);
	unsigned nSegs = (pl-1);
	// JPMDBG;

	srand48(seedVal);
	for (unsigned i = 0; i < nSegs; ++i)
	{
		unsigned j = (i + 1);
		emitSegment(
			ratePP[i],ratePP[j],
			points0[i],points0[j],
			points1[i] ,points1[j],
			userVectorPP[i] ,userVectorPP[j],
			radii[i] ,radii[j],
			normalSpeeds[i] ,normalSpeeds[j],
			tangentSpeeds[i] ,tangentSpeeds[j],
			orbitalSpeeds[i] ,orbitalSpeeds[j],
			inheritedVelocities[i] ,inheritedVelocities[j],
			vectorSpeeds[i] ,vectorSpeeds[j],
			speed,speedRandom,spread,dirV,
			outPos,
			outVel,
			outTime);
	}
	// JPMDBG;

	hOut.set( dOutput );
	data.setClean( plug );
	return( MS::kSuccess );
}


