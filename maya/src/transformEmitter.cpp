


#include <maya/MDataHandle.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MArrayDataBuilder.h>




#include "mayaFX.h"
#include "mayaMath.h"
#include "attrUtils.h"
#include "jMayaIds.h"
#include "transformEmitter.h"


MObject transformEmitter::aInput;
MObject transformEmitter::aMatrix;
MObject transformEmitter::aActive;
MTypeId transformEmitter::id( k_transformEmitter );


transformEmitter::transformEmitter()
{
}

transformEmitter::~transformEmitter()
{
}

void *transformEmitter::creator()
{
	return new transformEmitter;
}

MStatus transformEmitter::initialize()
{
	MFnNumericAttribute		nAttr;
	MFnCompoundAttribute		cAttr;

	MFnMatrixAttribute		mAttr;

	
	aMatrix = mAttr.create("transformMatrix","tmtx");
	mAttr.setReadable( false );
	mAttr.setStorable( true );

	aActive = nAttr.create( "transformActive", "tact",MFnNumericData::kBoolean);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(true);

	aInput = cAttr.create("transformInput", "tin");
	cAttr.addChild(aMatrix);
	cAttr.addChild(aActive);
	cAttr.setArray( true );
	addAttribute( aInput );

	return( MS::kSuccess );
}

MStatus transformEmitter::compute(const MPlug& plug, MDataBlock& data)
{
	// JPMDBG;

	// cerr << "transformEmitter::compute"  << endl;

	MStatus st;
	if( !(plug == mOutput) ) return( MS::kUnknownParameter );
	// cerr << "HERE 1" << endl;
	MArrayDataHandle hInput = data.inputArrayValue( aInput, &st );mser;
	// cerr << "HERE 2" << endl;
	unsigned count = hInput.elementCount(&st); mser;
	// cerr << "count" << count << endl;

	MVectorArray position;
	MVectorArray velocity;
	MVectorArray xaxis;
	MVectorArray yaxis;
	MVectorArray zaxis;
	MVectorArray phi;
	MVectorArray scale;
	MDoubleArray scaleX;



	for(unsigned i = 0;i < count; i++, hInput.next()) {
		// cerr << "-------------------- i" << i << endl;

		MDataHandle h = hInput.inputValue( &st );	mser;
		bool active = h.child(aActive).asBool();
		// cerr << "active  " << active << endl;

		if (active) {

			MMatrix mat(h.child(aMatrix).asMatrix());

					// cerr << "mat  " << mat << endl;

			MTransformationMatrix mtmat(mat);
			position.append(mtmat.getTranslation(MSpace::kWorld));
			velocity.append(MVector::zero);

			MQuaternion q = mtmat.rotation();
			double theta;
			MVector axa;
			q.getAxisAngle(axa, theta);
			axa *= theta;
			phi.append(axa);

			xaxis.append(MVector::xAxis.rotateBy(q));
			yaxis.append(MVector::yAxis.rotateBy(q));
			zaxis.append(MVector::zAxis.rotateBy(q));

			double scaled[3]  ;
			mtmat.getScale(scaled, MSpace::kWorld);
			scale.append(MVector(scaled));
			scaleX.append(scaled[0]);

		}
	}


	int multiIndex = plug.logicalIndex( &st);mser;
	MArrayDataHandle hOutArray = data.outputArrayValue(mOutput, &st);mser;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st);mser;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;
	MFnArrayAttrsData fnOutput;
	MObject dOutput = fnOutput.create ( &st );mser;

	//   arrays to append new particle data.
	MVectorArray outPos = fnOutput.vectorArray("position", &st);mser;
	MVectorArray outVel = fnOutput.vectorArray("velocity", &st);mser;
	MVectorArray outXaxis = fnOutput.vectorArray("xAxis", &st);mser;
	MVectorArray outYaxis = fnOutput.vectorArray("yAxis", &st);mser;
	MVectorArray outZaxis = fnOutput.vectorArray("zAxis", &st);mser;
	MVectorArray outPhi = fnOutput.vectorArray("phi", &st);mser;
	MVectorArray outScale = fnOutput.vectorArray("scalePP", &st);mser;
  MDoubleArray outScaleX = fnOutput.doubleArray("scaleX", &st);mser;

	// check time and isFull
	bool beenFull = isFullValue( multiIndex, data );
	if( beenFull ) return( MS::kSuccess );
	MTime cT = currentTimeValue( data );
	MTime sT = startTimeValue( multiIndex, data );
	MTime dT = deltaTimeValue( multiIndex, data );
	double dt =  dT.as(MTime::kSeconds);

	// per transform per frame
	unsigned rate = std::max(0, int(data.inputValue( mRate ).asDouble()) );
	unsigned pl = position.length();

	if( (cT <= sT) || (dT <= 0.0) || (!pl) || (rate <= 0 ) )
	{
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}

	if (rate >= 1) {
		outPos = position;
		outVel = velocity;
		outXaxis = xaxis;
		outYaxis = yaxis;
		outZaxis = zaxis;
		outPhi = phi;
		outScale = scale;
	  outScaleX = scaleX;
	}

	hOut.set( dOutput );
	data.setClean( plug );

	return( MS::kSuccess );
}


