#ifndef _splashTriData
#define _splashTriData

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MString.h>
#include <maya/MVector.h>
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MBoundingBox.h>

#include "splashTriBox.h"
#include "errorMacros.h"
// #include <algorithm>


/// This datatype is an implementation of Jon Louis Bently's kdTree as described in
/// K-d Trees for Semidynamic point sets  1990
/// This version supports boxsearching - i.e. different bounding boxes are stored
/// such that we can query all boxes which contain the given point


struct splashTriBoxKdNode  {
	bool bucket;							/// is this node a bucket
	bool empty;								/// is this node an empty bucket
	axis cutAxis;							/// if not, this axis will divide it 
	double cutVal;							/// at this value
	splashTriBoxKdNode *loChild, *hiChild;		/// and these pointers point to the children
	unsigned int loPoint, hiPoint;			/// Indices into the permutations array
	VECTOR_OF_TRIBOX_POINTERS overlapList;	/// list of splashTriBoxes whose boumding boxes intersect this bucket
};

class splashTriData 
{
	public:

		splashTriData();			

		~splashTriData();  
		const splashTriBoxKdNode * root();
		splashTriData& operator=(const splashTriData & otherData );

		void 		build(); 							/// build root from _perm

		splashTriBoxKdNode * 	build( int low,  int high ); 	/// build children recursively
		int size() ;
		//MStatus 	getSize(int &siz);  			/// size of _perm and _prismVector - returns failure if they are different

		void  		setMaxPointsPerBucket( int b) {if (b < 1) {_maxPointsPerBucket = 1;} else {_maxPointsPerBucket = b;}}

		axis 		findMaxAxis(const  int low,const  int  high) const;

		MStatus 	init();

		MStatus 	addTriBoxes(const MFnDynSweptGeometryData  &g1, unsigned el)  ;
		MStatus 	addTriBoxes(const MFnDynSweptGeometryData  &g1, unsigned el, const MBoundingBox & box)  ;
	
		//void quickPartition (  int left,  int right,  int wanted, axis cutAxis );
		void wirthSelect(  int left,  int right,  int k, axis cutAxis )  ;

		void makeEmpty() ; 
		
		void makeEmpty(splashTriBoxKdNode * p) ; 

		void  setOverlapList(splashTriBoxKdNode * p,  splashTriBox * sph  );

		void sortBoxPtrs ( VECTOR_OF_TRIBOX_POINTERS * p);
		
		void sortBoxPtrs ( int left,  int right, VECTOR_OF_TRIBOX_POINTERS * p);

		// void  boxSearch( splashTriBox *, VECTOR_OF_TRIBOX_POINTERS * resultBoxes) ;

		void  boxSearch(const splashTriBoxKdNode * p, splashTriBox *, VECTOR_OF_TRIBOX_POINTERS * resultBoxes);

		void  makeResultBoxes(const VECTOR_OF_TRIBOX_POINTERS * overlapList, splashTriBox * searchBox,VECTOR_OF_TRIBOX_POINTERS *resultBoxes) const ;

		// VECTOR_OF_TRIBOXES & triangleList() ;
		
		VECTOR_OF_TRIBOX_POINTERS & triangleList();
	
	private:
	

		// VECTOR_OF_TRIBOXES * _splashTriBoxVector;				/// pointer to simple vector of splashTriBoxes

		VECTOR_OF_TRIBOX_POINTERS * _perm;				/// pointer to (permutations) - a list of pointers to elements of _splashTriBoxVector

		splashTriBoxKdNode * _pRoot;							/// pointer to root of tree

		unsigned int _maxPointsPerBucket;				/// at build time keep recursing until no bucket holds more than this
		
		// void sortResultBoxes( int left,  int right , VECTOR_OF_TRIBOX_POINTERS * resultBoxes);	/// sort pointers so we can remove duplicates
} ;

#endif


