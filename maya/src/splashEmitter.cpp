

#include <maya/MIOStream.h>
#include <math.h>
//#include <stdlib.h>
#include "splashTriData.h"
// #include "tritri.h"
#include "splashEmitter.h"
#include "mayaMath.h"

#include "lookup.h"

#include <maya/MDataHandle.h>
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MPlugArray.h>

#include <maya/MQuaternion.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>


#include <maya/MFnDependencyNode.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnAnimCurve.h>

#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MGlobal.h>
#include <maya/MBoundingBox.h>
#include <maya/MArrayDataBuilder.h>


#include "attrUtils.h"

#include "jMayaIds.h"

const double EPSILON = 0.0001;

const int  MAXPOINTSPERBUCKET  = 2;

MObject splashEmitter::aSweptGeometry;

MObject splashEmitter::aInheritXVelocity;

MObject splashEmitter::aInheritAvgVelocity;

MObject splashEmitter::aInheritPressure;

MObject splashEmitter::aSpeedThreshold;

MObject splashEmitter::aAllowSelfIntersections;

MObject splashEmitter::aFacingRemap;

MObject splashEmitter::aPressureRemap;

MObject splashEmitter::aMaxTimeStep;

MObject splashEmitter::aBBMinX;
MObject splashEmitter::aBBMinY;
MObject splashEmitter::aBBMinZ;
MObject splashEmitter::aBBMin;
MObject splashEmitter::aBBMaxX;
MObject splashEmitter::aBBMaxY;
MObject splashEmitter::aBBMaxZ;
MObject splashEmitter::aBBMax;
MObject splashEmitter::aBBSizX;
MObject splashEmitter::aBBSizY;
MObject splashEmitter::aBBSizZ;
MObject splashEmitter::aBBSiz;
MObject splashEmitter::aBB;
MObject splashEmitter::aUseBB;

MObject splashEmitter::aSamplePoints;
MObject splashEmitter::aSampleVelocities;
MObject splashEmitter::aSampleMasses;
MObject splashEmitter::aSampleDeltaTime;
MObject splashEmitter::aSampleFieldData;
MObject splashEmitter::aForces;



//MObject splashEmitter::aMinPressure;
//MObject splashEmitter::aMaxPressure;
MObject splashEmitter::aVerbose;
MTypeId splashEmitter::id(  k_splashEmitter );

splashEmitter::splashEmitter(){}

splashEmitter::~splashEmitter(){}

void *splashEmitter::creator()
{
    return new splashEmitter;
}



MStatus splashEmitter::initialize()

{
	MStatus st;
	MString method("splashEmitter::initialize");
	
	MFnTypedAttribute tAttr;
	MFnMessageAttribute mAttr;
	MFnNumericAttribute nAttr;
	MFnCompoundAttribute cAttr;
	MFnUnitAttribute uAttr;
	
	
	// GEOMETRY
	aSweptGeometry = tAttr.create("geometries", "geos", MFnData::kDynSweptGeometry);
	tAttr.setReadable(false);
	tAttr.setArray(true);
	tAttr.setIndexMatters(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kDelete);
	st = addAttribute( aSweptGeometry ); mser
	
	aInheritXVelocity =  nAttr.create( "inheritXVelocity", "ixv", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aInheritXVelocity);	mser;
	
	aInheritAvgVelocity =  nAttr.create( "inheritAvgVelocity", "iav", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aInheritAvgVelocity);	mser;
	
	aInheritPressure =  nAttr.create( "inheritPressure", "ipr", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aInheritPressure);	mser;
	
	aSpeedThreshold =  nAttr.create( "speedThreshold", "sth", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aSpeedThreshold);	mser;
	
	aAllowSelfIntersections =  nAttr.create( "allowSelfIntersections", "asi", MFnNumericData::kBoolean); 
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(false);
	st =	addAttribute(aAllowSelfIntersections);	mser;
	
	aVerbose =  nAttr.create( "verbose", "vbs", MFnNumericData::kBoolean); 
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(false);
	st =	addAttribute(aVerbose);	mser;
	
	
	aFacingRemap = mAttr.create("facingRemap", "frm",  &st ); mser;
	st = addAttribute(aFacingRemap); mser;
	aPressureRemap = mAttr.create("pressureRemap", "prm",  &st ); mser;
	st = addAttribute(aPressureRemap); mser;
	
	///////////////////////////////////////////////////////////////////////
	aMaxTimeStep = uAttr.create( "maxTimeStep", "mxt", MFnUnitAttribute::kTime );
	uAttr.setStorable(true);
	uAttr.setDefault(1.0);
	st =  addAttribute(aMaxTimeStep);  mser;
	
	
	
	aBBMinX = nAttr.create( "boxMinX", "bnx", MFnNumericData::kDouble);
	aBBMinY = nAttr.create( "boxMinY", "bny", MFnNumericData::kDouble);
	aBBMinZ = nAttr.create( "boxMinZ", "bnz", MFnNumericData::kDouble);
	aBBMin = nAttr.create( "boxMin", "bn", aBBMinX, aBBMinY, aBBMinZ );
	nAttr.setHidden(false);
	st = addAttribute(aBBMinX);
	st = addAttribute(aBBMinY);
	st = addAttribute(aBBMinZ);
	st = addAttribute(aBBMin);
	aBBMaxX = nAttr.create( "boxMaxX", "bxx", MFnNumericData::kDouble);
	aBBMaxY = nAttr.create( "boxMaxY", "bxy", MFnNumericData::kDouble);
	aBBMaxZ = nAttr.create( "boxMaxZ", "bxz", MFnNumericData::kDouble);
	aBBMax = nAttr.create( "boxMax", "bx", aBBMaxX,  aBBMaxY, aBBMaxZ );
	nAttr.setHidden(false);
	st = addAttribute(aBBMaxX);
	st = addAttribute(aBBMaxY);
	st = addAttribute(aBBMaxZ);
	st = addAttribute(aBBMax);
	aBBSizX = nAttr.create( "boxSizX", "bsx", MFnNumericData::kDouble);
	aBBSizY = nAttr.create( "boxSizY", "bsy", MFnNumericData::kDouble);
	aBBSizZ = nAttr.create( "boxSizZ", "bsz", MFnNumericData::kDouble);
	aBBSiz = nAttr.create( "boxSiz", "bs", aBBSizX,  aBBSizY, aBBSizZ );
	nAttr.setHidden(false);
	st = addAttribute(aBBSizX);
	st = addAttribute(aBBSizY);
	st = addAttribute(aBBSizZ);
	st = addAttribute(aBBSiz);
	aBB = cAttr.create("inBoundingBox", "ibb");
	cAttr.addChild(aBBMin);
	cAttr.addChild(aBBMax);
	cAttr.addChild(aBBSiz);
	cAttr.setStorable(true);
	
	cAttr.setHidden(false);
	
	addAttribute(aBB);
	
	aUseBB =  nAttr.create( "useBoundingBox", "ubb", MFnNumericData::kBoolean); 
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(false);
	st =	addAttribute(aUseBB);	mser;
	



	aSamplePoints = tAttr.create("samplePoints", "spts", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleVelocities = tAttr.create("sampleVelocities", "svls", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleMasses = tAttr.create("sampleMasses", "smss", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);

 	aSampleDeltaTime = uAttr.create( "sampleDeltaTime", "sdt", MFnUnitAttribute::kTime, 0.0, &st ); mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleFieldData = cAttr.create("sampleFieldData","sfd");
	cAttr.addChild(aSamplePoints);
	cAttr.addChild(aSampleVelocities);
	cAttr.addChild(aSampleMasses);
	cAttr.addChild(aSampleDeltaTime);

	//st = addAttribute(aSamplePoints );mser;
   //st = addAttribute(aSampleVelocities );mser;
   //st = addAttribute(aSampleMasses );mser;
	//st = addAttribute(aSampleDeltaTime);mser;
	st = addAttribute(aSampleFieldData);mser;


	aForces = tAttr.create("forces", "frc", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setArray(true);
	cAttr.setIndexMatters(false);
	cAttr.setDisconnectBehavior(MFnAttribute::kDelete);
   st = addAttribute( aForces ); mser;

	attributeAffects(mCurrentTime,aSamplePoints );
	attributeAffects(mCurrentTime,aSampleVelocities);
	attributeAffects(mCurrentTime,aSampleMasses);
	attributeAffects(mCurrentTime,aSampleFieldData);


	/*
	aMinPressure =  nAttr.create( "minPressure", "mnp", MFnNumericData::kFloat ); mser;
	nAttr.setStorable(false);
	nAttr.setReadable(true);
	st =	addAttribute(aMinPressure);	mser;
	
	aMaxPressure =  nAttr.create( "maxPressure", "mxp", MFnNumericData::kFloat ); mser;
	nAttr.setStorable(false);
	nAttr.setReadable(true);
	st =	addAttribute(aMaxPressure);	mser;
*/


	
	return( MS::kSuccess );
}


MStatus splashEmitter::compute(const MPlug& plug, MDataBlock& data)
{
	
 	MStatus st;
	//unsigned int counter = 0;
	MString method("splashEmitter::compute");
	unsigned int i=0;
	
	
	////////////////////////////////////////////////////////
	if( !(plug == mOutput) ) return( MS::kUnknownParameter );
	if ( data.inputValue( state ).asShort() == 1 ) return MS::kSuccess;

	int multiIndex = plug.logicalIndex( &st);mser;
	bool beenFull = isFullValue( multiIndex, data );
	if( beenFull )return( MS::kSuccess );
	bool allowSelfIntersect = data.inputValue(aAllowSelfIntersections).asBool();
	bool useBB = data.inputValue(aUseBB).asBool();
	bool verbose = data.inputValue(aVerbose).asBool();
	////////////////////////////////////////////////////////


	// create output object
	////////////////////////////////////////////////////////
	MArrayDataHandle hOutArray = data.outputArrayValue(mOutput, &st);mser;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st);mser;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;
	MFnArrayAttrsData fnOutput;
    MObject dOutput = fnOutput.create ( &st );mser;
	////////////////////////////////////////////////////////

	// check time and isFull
	///////////////////////////////////////////////////////////////////
	MTime cT = currentTimeValue( data );
	MTime sT = startTimeValue( multiIndex, data );
	MTime dT = deltaTimeValue( multiIndex, data );
	MTime maxTimeStep =  data.inputValue( aMaxTimeStep).asTime();
	if( (cT <= sT) || (dT <= 0.0)  || (dT > maxTimeStep))
	{
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}
	///////////////////////////////////////////////////////////////////


	// 
	///////////////////////////////////////////////////////////////////
	MFnDependencyNode thisNodeFn(thisMObject());
	MObject  aSpread = thisNodeFn.attribute(MString("spread"));
	MObject  aNormalSpeed = thisNodeFn.attribute(MString("normalSpeed"));
	MObject  aSpeedRandom = thisNodeFn.attribute(MString("speedRandom"));
	
	// position and velocity arrays to append new particle data.
	MVectorArray outPos = fnOutput.vectorArray("position", &st);mser;
	MVectorArray outVel = fnOutput.vectorArray("velocity", &st);mser;
	MDoubleArray outTim = fnOutput.doubleArray("timeInStep", &st);mser;
	
	// Get attributes.
	long seedVal = seedValue( multiIndex, data );
	
	const long & ignoreSeed = -1;
	double rate = doubleValue( data, mRate);
	double speed = doubleValue( data, mSpeed );
	double speedRandom = doubleValue( data, aSpeedRandom );
	double normalSpeed = doubleValue( data, aNormalSpeed );
	double spread = doubleValue( data, aSpread );
	double inheritXVel = doubleValue( data, aInheritXVelocity );
	double inheritAVel = doubleValue( data, aInheritAvgVelocity );
	double inheritPressure = doubleValue( data, aInheritPressure);
	double speedThresh = doubleValue( data, aSpeedThreshold );
	if (spread > 1 ) spread = 1;
	if (spread < 0 ) spread = 0;
	
	
	// we want to evaluate only triangles in this box
	double bbminx = data.inputValue(aBBMinX ).asDouble();
	double bbminy = data.inputValue(aBBMinY ).asDouble();
	double bbminz = data.inputValue(aBBMinZ ).asDouble();
	double bbmaxx = data.inputValue(aBBMaxX ).asDouble();
	double bbmaxy = data.inputValue(aBBMaxY ).asDouble();
	double bbmaxz = data.inputValue(aBBMaxZ ).asDouble();
	MPoint p1(bbminx, bbminy, bbminz);
	MPoint p2(bbmaxx, bbmaxy, bbmaxz);
	MBoundingBox box = MBoundingBox(p1,p2);
	
	MVector dirV = vectorValue( data, mDirection );

	double dt = dT.as( MTime::kSeconds );	
	///////////////////////////////////////////////////////////////////
	
	// swept geometry to triTree
	///////////////////////////////////////////////////////////////////
	MArrayDataHandle hSweptGeometryArray = data.inputArrayValue( aSweptGeometry, &st ); mser;
	unsigned int count = hSweptGeometryArray.elementCount(&st); mser;
	if (!count) {return MS::kFailure;}
	splashTriData *triTree = new splashTriData();
	MFnDynSweptGeometryData fnSweptGeometry1;
	unsigned el = 0;
	do { 
		//unsigned int el = hSweptGeometryArray.elementIndex(&st);  mser;
		if (st == MS::kSuccess) {
			MDataHandle hSweptGeometry = hSweptGeometryArray.inputValue( &st );mser;	
			MObject  dSweptGeometry = hSweptGeometry.data();
			fnSweptGeometry1.setObject(dSweptGeometry);
			if (useBB) {
				st = triTree->addTriBoxes(fnSweptGeometry1, el,box);mser;
			} else {
				st = triTree->addTriBoxes(fnSweptGeometry1, el);  // mser;
			}
			el++;
		}
	} while (hSweptGeometryArray.next() == MS::kSuccess );
	///////////////////////////////////////////////////////////////////
	
	srand48(seedVal);
	///////////////////////////////////////////////////////
	
	// Get facing curve stuff
	//////////////////////////////////////////////////////////
	lookup facingLookup;
	MFnAnimCurve facingLookupAniFn; 
	st = getAniCurveFn(thisMObject(), aFacingRemap, facingLookupAniFn);
	if (!(st.error())) {
		facingLookup.create(facingLookupAniFn,100,true,true,  -1.0,1.0);
	} 
	
	// Get pressure curve stuff
	//////////////////////////////////////////////////////////
	lookup pressureLookup;
	MFnAnimCurve pressureLookupAniFn; 
	st = getAniCurveFn(thisMObject(), aPressureRemap, pressureLookupAniFn);
	if (!(st.error())) {
		pressureLookup.create(pressureLookupAniFn,100,false,false);
	} 

	// 
	///////////////////////////////////////////////////////
	triTree->setMaxPointsPerBucket(MAXPOINTSPERBUCKET);
	triTree->build();
	
	
	VECTOR_OF_TRIBOX_POINTERS & perm = triTree->triangleList();
	VECTOR_OF_TRIBOX_POINTERS::iterator currentTriBox = perm.begin();
	int tcount = 0;
	
	float minPressure = 1e+38f;
	float maxPressure = -minPressure;
	
	bool recordedPressure=false;
	
	//bool intersected = 0;
	//cerr << "about to loop"<<endl;
	
	// things to collect
	
	
	
	
	while (currentTriBox != perm.end()){
		
		VECTOR_OF_TRIBOX_POINTERS * resultBoxes = new VECTOR_OF_TRIBOX_POINTERS;
		if ((*currentTriBox)->isMoving()){

			triTree->boxSearch(triTree->root(), (*currentTriBox) , resultBoxes);

			triTree->sortBoxPtrs(resultBoxes);
			splashTriBox * lastBox = 0;
			VECTOR_OF_TRIBOX_POINTERS::iterator currentResultBoxPtr = resultBoxes->begin();		
			while (currentResultBoxPtr != resultBoxes->end()){
				if ((*currentResultBoxPtr) != lastBox) {			
					MPoint start(MPoint::origin);
					MPoint end(MPoint::origin);
					if (( (*currentTriBox)->id() != (*currentResultBoxPtr)->id() ) || (allowSelfIntersect)) {
						bool intersects = (*currentTriBox)->trianglesIntersect((*currentResultBoxPtr), start, end);
						
						if (intersects) {
							
							
							const MVector & ch1 = (*currentTriBox)->change() ;
							const MVector & ch2 = (*currentResultBoxPtr)->change() ;
							MVector relVelocity =  ch1-ch2 / dt ;
							
							double relSpeed = relVelocity.length();
							relSpeed = relSpeed - speedThresh;
							if (relSpeed>EPSILON) {
								const MVector & n1 = (*currentTriBox)->normal();
								const MVector & n2 = (*currentResultBoxPtr)->normal();
								
								double dotFactor = double(facingLookup.evaluate( float(n1*n2)));
								
								
								float pressure = float((relVelocity*(n1)) + (-relVelocity*(n2)));
								// cerr << "pressure " << pressure << endl;
								if (verbose) {
									if (pressure > maxPressure) maxPressure = pressure;
									if (pressure < minPressure) minPressure = pressure;
									recordedPressure = true;
								}
								double pressureRateFactor = double(pressureLookup.evaluate( pressure));
								
								
								double isectLen = (end-start).length();
								double doubleCount = fabs(pressureRateFactor * dotFactor * rate * isectLen * relSpeed);
								const unsigned int intCount = mayaMath::randomPoints(doubleCount, start, end, outPos, outTim, ignoreSeed);
								
								MVector xVel(0.0,0.0,0.0);
								MVector pVel(0.0,0.0,0.0);
								MVector aVel(0.0,0.0,0.0);
								MVector nVel(0.0,0.0,0.0);
								
								if ((0.0 != inheritXVel) || (0.0 != inheritPressure)) {
									(*currentTriBox)->intersectionDirection(*currentResultBoxPtr, xVel);
									xVel = (xVel / dt) ;
									pVel =  xVel;
									xVel = xVel * inheritXVel;
									pVel = pVel * (inheritPressure * double(pressure)) ;
								}
								
								if (inheritAVel) aVel = ( (ch1+ch2) / (2 * dt) )  * inheritAVel; // average velocity of tris 
								if (normalSpeed) nVel = (normalSpeed *(n2+n1));
								
								MVectorArray newVels;  
								
								MVector newVec(dirV + xVel + aVel + nVel + pVel);
								
								newVec *= speed;
								
								if (spread) {
									mayaMath::sphericalSpread(newVec,spread,intCount, newVels, ignoreSeed);
								} else {
									newVels.setLength(intCount);
									for(i=0;i < intCount;i++){
										newVels.set(newVec,i);
									}
								}
								if (speedRandom) {
									mayaMath::randomizeLength(speedRandom, newVels,ignoreSeed);
								}
								
								for (i=0;i<intCount;i++) outVel.append(newVels[i]);
								// for (i=0;i<intCount;i++) outTim.append(0.0);
							}
						}
					}	
				}
				lastBox = (*currentResultBoxPtr);
				currentResultBoxPtr++;
			}
			delete resultBoxes;resultBoxes=0;
		}
		tcount++;
		currentTriBox++;
	}
	
	if (verbose && recordedPressure) {
		MString infostr("Pressure range: ");
		infostr += double(minPressure);
		infostr += MString(" to ");
		infostr += double(maxPressure);
		MGlobal::displayInfo(infostr);
	}
	delete triTree;
	
	
	MVectorArray appliedForce;

	/// if there are any forces connected, we want to sample them and add in the result

    unsigned n = outVel.length();
    MDoubleArray mas(n,1.0); 
   st = getAppliedForces(data,outPos ,outVel ,mas ,dT,appliedForce);
	
  if (n &&  (!st.error())  && (appliedForce.length() == n )) {

    	for(unsigned i = 0; i < n; i++ )
    	{
 		outVel[i] += appliedForce[i];
 //  			outVel[i] += MVector::zero;
    	}
   }
	
	hOut.set( dOutput );
	data.setClean( plug );

	return( MS::kSuccess );
}


// Force Accumulator procedure for gathering maya's force fields
MStatus splashEmitter::getAppliedForces(
	 MDataBlock& block,
	 const MVectorArray &positions,
	 const MVectorArray &velocities,
	 const MDoubleArray &masses,
	 const MTime &dT,
	 MVectorArray &appliedForce
	 )
{
	
	MStatus st;
	// dont do anything if no points
	unsigned siz = positions.length();
	if (siz==0){return MS::kUnknownParameter;}
	
	// dont do anything if no forces
	MPlug forcesPlug(thisMObject(), aForces);  //  force plug
	unsigned numForces = forcesPlug.numElements(&st);if (st.error()){return st;}
	if (! numForces) {return MS::kUnknownParameter;}
	
	// note - we haven't checked the length of arrays, and indeed the masses (densities) array
	// will be empty - lets see if the forces complain -hopefully they will assume a value of 1.
	
	//unsigned int counter = 0;
	appliedForce = MVectorArray(siz, MVector::zero);
	MDataHandle hSampleFieldData = block.outputValue(aSampleFieldData, &st );if (st.error()){return st;}
	MDataHandle hSamplePoints = hSampleFieldData.child(aSamplePoints);
	MDataHandle hSampleVelocities = hSampleFieldData.child(aSampleVelocities );
	MDataHandle hSampleMasses = hSampleFieldData.child(aSampleMasses );
	MDataHandle hDeltaTime = hSampleFieldData.child(aSampleDeltaTime );

	MFnVectorArrayData fnSamplePoints;
	MObject dSamplePoints = fnSamplePoints.create( positions, &st ); ;if (st.error()){return st;}
	MFnVectorArrayData fnSampleVelocities;
	MObject dSampleVelocities = fnSampleVelocities.create( velocities, &st ); ;if (st.error()){return st;}
	MFnDoubleArrayData fnSampleMasses;
	MObject dSampleMasses = fnSampleMasses.create( masses, &st ); ;if (st.error()){return st;}

	hSamplePoints.set(dSamplePoints);
	hSampleVelocities.set(dSampleVelocities);
	hSampleMasses.set(dSampleMasses);
	hDeltaTime.set(dT);

	block.setClean(aSamplePoints);
	block.setClean(aSampleVelocities);
	block.setClean(aSampleMasses);
	block.setClean(aSampleDeltaTime);
	block.setClean(aSampleFieldData);


	for (unsigned nf = 0;nf<numForces;nf++){
		MPlug tmpForcePlug = forcesPlug.elementByPhysicalIndex(nf,&st);if (st.error()){continue;}
		if (tmpForcePlug.isConnected()) {
			MObject tmpForceObject;
			st = tmpForcePlug.getValue(tmpForceObject); if (st.error()){continue;}
			MFnVectorArrayData tmpForceFn(tmpForceObject);
			MVectorArray tmpForce = tmpForceFn.array(&st);if (st.error()){continue;}
			if (tmpForce.length() == siz) {
				for (unsigned t=0; t < siz; t++ ) {
					appliedForce[t] += tmpForce[t];
				}
			}
		}
	}	
	
	
	return MS::kSuccess;
}
