

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MDataHandle.h>
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MPlugArray.h>
#include <maya/MRenderUtil.h> 
#include <maya/MQuaternion.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MArrayDataBuilder.h>

#include "mayaFX.h"
#include "mayaMath.h"
#include "attrUtils.h"
#include "jMayaIds.h"
#include "ppEmitter.h"

	

MObject ppEmitter::aPoints;
MObject ppEmitter::aVelocities;
MObject ppEmitter::aEmissionVectors;
MObject ppEmitter::aEmissionRadius;
MObject ppEmitter::aEmissionRadiusBias;
MObject ppEmitter::aEmissionRadiusBiasPP;


MObject ppEmitter::aEmissionShape;
//MObject ppEmitter::aEmissionShapePP;
MObject ppEmitter::aEmissionShapeAxis;
MObject ppEmitter::aEmissionShapeAxisPP;
MObject ppEmitter::aEmissionRadii;

MObject ppEmitter::aSweepShutterOpen;


MObject ppEmitter::aParentIds;
MObject ppEmitter::aColors;
MObject ppEmitter::aRadius;
MObject ppEmitter::aRadii;
MObject ppEmitter::aRadiusVariation;
MObject ppEmitter::aNormalSpeeds;
MObject ppEmitter::aVectorSpeed;
MObject ppEmitter::aSpreads;
MObject ppEmitter::aSpeedRandoms;

MObject ppEmitter::aColorVariation;




// MObject ppEmitter::aSweepTimeOffset;
const double  PI  = 3.1415927;
const double  TAU = 2.0 * PI;



MTypeId ppEmitter::id( k_ppEmitter );


ppEmitter::ppEmitter()
{
}

ppEmitter::~ppEmitter()
{
}

void *ppEmitter::creator()
{
	return new ppEmitter;
}


// unsigned ppEmitter::foo() {}

unsigned ppEmitter::emitSphereFromPoint(
	double dt,
	double sweepOpen,
	double rate,
	const MVector &point,
	const MVector &velocity,
	const MVector &emissionVector,
	double emissionRadius,
	double sphBiasVal,
	double parentId ,
	const MVector &color ,
	double radius,
	double normalSpeed,
	double spread,
	double speedRandom,
	double speed,
	const MVector & colorVariation,
	double radiusVariation,
	MVectorArray &outPos,
	MVectorArray &outVel,
	MDoubleArray &outTime,
	MDoubleArray &outParentRadius,
	MDoubleArray &outParentParticleId,
	MVectorArray &outParentColor
) 
{

	if (sphBiasVal < 0 ) sphBiasVal = 0;
	if (sphBiasVal > 4 ) sphBiasVal = 4;
	

	if (spread > 1.0 ) spread = 1.0;
	if (spread < 0.0 ) spread = 0.0;

	// stuff needed for spread 
	//////////////////////////////////
	double rot_min = cos(spread*PI);
	double rot_t, rot_w, rot_z;
	double rot_range = 1.0 - rot_min;
	//////////////////////////////////

	// double emissionDiameter = emissionRadius * 2.0;
	if (rate <= 0.0) return 0;
	unsigned int nRate = mayaMath::randCount(rate);	
	if (nRate <= 0) return 0;

	MVector point0 = point - (velocity * dt);


	// MVector sweepPointA, sweepPointB, sweepPoint, radPoint;
	MVector emissionVec;
	// float radius;
	MVector sweepPoint;
	MVector radPoint;
	// MVector line = (points1B - points1A);
	// MVector tangent  =  line.normal();
	// find rotation of Z axis to tangent vector
	// cerr << "nRate = " << nRate << endl; 
	

	unsigned i = 0; 
	while (i < nRate) {
		// //// // JPMDBG;
		// double x = ((drand48() *emissionDiameter) - emissionRadius);
		// double y = ((drand48() *emissionDiameter) - emissionRadius);
		// double z = ((drand48() *emissionDiameter) - emissionRadius);


		double x = 0.0;
		double y = 0.0;
		double z = 0.0;
		double sqrad = 0.0;
		if (emissionRadius > 0.0){
			x = ((drand48() * 2.0) - 1.0);
			y = ((drand48() * 2.0) - 1.0);
			z = ((drand48() * 2.0) - 1.0);
			sqrad = ( x*x + y*y + z*z) ;
		}
		// rad = ( x*x + y*y + z*z);
		MVector nVec(MVector::zero);


		if (sqrad  <= 1.0 ) {
			MVector normalVelocity(MVector::zero);
			if  (emissionRadius > 0.0){
				nVec = MVector(x,y,z);
				if (sphBiasVal == 0) { 
					nVec.normalize();
				} else if (sphBiasVal != 1.0) {
					double nlen =  nVec.length();
					nlen = pow(nlen, sphBiasVal);
					nVec = nVec.normal() * nlen;
				}
				nVec *= emissionRadius;
				normalVelocity = nVec.normal() * normalSpeed;
			}

			//// JPMDBG;
			// MVector nVec(x,y,z);
			// work out the emission point.
			double sweep = (drand48() * sweepOpen) + 0.5;	// time	
			double oneMinusSweep = 1.0 - sweep;
			sweepPoint = ((point * sweep) +  (point0 * oneMinusSweep));
			radPoint = sweepPoint + nVec;
			// JPMDBG;

			MVector vec = emissionVector + normalVelocity;
			// JPMDBG;

			MQuaternion qr(MVector::zAxis, vec);
			// JPMDBG;
			MVector rot_v;
	    	rot_z = (drand48() * rot_range) + rot_min;
	    	rot_t = drand48() * TAU;
	    	rot_w = sqrt( 1.0 - rot_z*rot_z );
	    	rot_v.z = rot_z;
	    	rot_v.x = rot_w * cos( rot_t );
	    	rot_v.y = rot_w * sin( rot_t );
	    	rot_v = rot_v.rotateBy(qr);
			// // JPMDBG;

			// we randomize speed
			double randSpeed = speed + (speedRandom * ((  drand48() * 2.0)-1.0)  );
    		vec =  rot_v * vec.length() * randSpeed;
		//// JPMDBG;


    		outVel.append(vec);

    		// particles that were emitted a little while ago will
    		// have travelled a bit. 
			outPos.append( radPoint + (vec * oneMinusSweep * dt)) ;	
			outTime.append( sweep);
		//// JPMDBG;

			double thisRadius = radius + (radiusVariation * ((drand48() * 2.0)-1.0));
			outParentRadius.append(thisRadius);
			MVector outCol;
			outCol.x = color.x + (colorVariation.x  * ((drand48() * 2.0)-1.0));
			outCol.y = color.y + (colorVariation.y  * ((drand48() * 2.0)-1.0));
			outCol.z = color.z + (colorVariation.z  * ((drand48() * 2.0)-1.0));
			outParentColor.append(outCol);
		//// JPMDBG;
			outParentParticleId.append(parentId);
			++i;
		}
	}
			//// JPMDBG;

	return nRate;
}

unsigned ppEmitter::emitCircleFromPoint(
	double dt,
	double sweepOpen,
	double rate,
	const MVector &point,
	const MVector &velocity,
	const MVector &emissionVector,
	const MVector & emissionShapeAxis,
	double emissionRadius,
	double sphBiasVal,
	double parentId ,
	const MVector &color ,
	double radius,
	double normalSpeed,
	double spread,
	double speedRandom,
	double speed,
	const MVector & colorVariation,
	double radiusVariation,
	MVectorArray &outPos,
	MVectorArray &outVel,
	MDoubleArray &outTime,
	MDoubleArray &outParentRadius,
	MDoubleArray &outParentParticleId,
	MVectorArray &outParentColor
) 
{

	if (sphBiasVal < 0 ) sphBiasVal = 0;
	if (sphBiasVal > 4 ) sphBiasVal = 4;
	

	if (spread > 1.0 ) spread = 1.0;
	if (spread < 0.0 ) spread = 0.0;

	// stuff needed for spread 
	//////////////////////////////////
	double rot_min = cos(spread*PI);
	double rot_t, rot_w, rot_z;
	double rot_range = 1.0 - rot_min;
	//////////////////////////////////

	// double emissionDiameter = emissionRadius * 2.0;
	if (rate <= 0.0) return 0;
	unsigned int nRate = mayaMath::randCount(rate);	
	if (nRate <= 0) return 0;

	MVector point0 = point - (velocity * dt);


	// MVector sweepPointA, sweepPointB, sweepPoint, radPoint;
	MVector emissionVec;
	// float radius;
	MVector sweepPoint;
	MVector radPoint;
	// MVector line = (points1B - points1A);
	// MVector tangent  =  line.normal();
	// find rotation of Z axis to tangent vector
	// cerr << "nRate = " << nRate << endl; 
	
	// for emissionShapeAxis, create quaternion to rotate
	// z azis to emission shape axis.
	// cerr << "emissionShapeAxis:" << emissionShapeAxis << endl;

	MQuaternion shapeOrientation(MVector::zAxis, emissionShapeAxis.normal());


	unsigned i = 0; 
	while (i < nRate) {
		// //// // JPMDBG;
		// double x = ((drand48() *emissionDiameter) - emissionRadius);
		// double y = ((drand48() *emissionDiameter) - emissionRadius);
		// double z = ((drand48() *emissionDiameter) - emissionRadius);

		// make rand points in circle
		double x = 0.0;
		double y = 0.0;
		double sqrad = 0.0;
		if (emissionRadius > 0.0){
			x = ((drand48() * 2.0) - 1.0);
			y = ((drand48() * 2.0) - 1.0);
			sqrad = ( x*x + y*y) ;
		}
		// rad = ( x*x + y*y + z*z);
		

		if (sqrad  <= 1.0 ) {
			MVector nVec(MVector::zero);
			MVector normalVelocity(MVector::zero);

			if  (emissionRadius > 0.0){
				nVec = MVector(x,y,0.0);
				if (sphBiasVal == 0) { 
					nVec.normalize();
				} else if (sphBiasVal != 1.0) {
					double nlen =  nVec.length();
					nlen = pow(nlen, sphBiasVal);
					nVec = nVec.normal() * nlen;
				}

				nVec *= emissionRadius;
				nVec = nVec.rotateBy(shapeOrientation);
				
				normalVelocity = nVec.normal() * normalSpeed;
			}


			//// JPMDBG;
			// MVector nVec(x,y,z);
			// work out the emission point.
			double sweep = (drand48() * sweepOpen) + 0.5;	// time	
			double oneMinusSweep = 1.0 - sweep;
			sweepPoint = ((point * sweep) +  (point0 * oneMinusSweep));
			radPoint = sweepPoint + nVec;

			MVector vec = emissionVector + normalVelocity;

			MQuaternion qr(MVector::zAxis, vec);
			// JPMDBG;
			MVector rot_v;
	    	rot_z = (drand48() * rot_range) + rot_min;
	    	rot_t = drand48() * TAU;
	    	rot_w = sqrt( 1.0 - rot_z*rot_z );
	    	rot_v.z = rot_z;
	    	rot_v.x = rot_w * cos( rot_t );
	    	rot_v.y = rot_w * sin( rot_t );
	    	rot_v = rot_v.rotateBy(qr);
			// // JPMDBG;

			// we randomize speed
			double randSpeed = speed + (speedRandom * ((  drand48() * 2.0)-1.0)  );
    		vec =  rot_v * vec.length() * randSpeed;
		//// JPMDBG;


    		outVel.append(vec);

    		// particles that were emitted a little while ago will
    		// have travelled a bit. 
			outPos.append( radPoint + (vec * oneMinusSweep * dt)) ;	
			outTime.append( sweep);
		//// JPMDBG;

			double thisRadius = radius + (radiusVariation * ((drand48() * 2.0)-1.0));
			outParentRadius.append(thisRadius);
			MVector outCol;
			outCol.x = color.x + (colorVariation.x  * ((drand48() * 2.0)-1.0));
			outCol.y = color.y + (colorVariation.y  * ((drand48() * 2.0)-1.0));
			outCol.z = color.z + (colorVariation.z  * ((drand48() * 2.0)-1.0));
			outParentColor.append(outCol);
		//// JPMDBG;
			outParentParticleId.append(parentId);
			++i;
		}
	}
			//// JPMDBG;

	return nRate;
}





MStatus ppEmitter::initialize()

{
	MStatus st;
//  unsigned int counter = 0;
	MString method("ppEmitter::initialize");

	MFnTypedAttribute tAttr;
	MFnCompoundAttribute cAttr;
	MFnUnitAttribute uAttr;
	MFnEnumAttribute eAttr;
	MFnNumericAttribute nAttr;



	aPoints = tAttr.create("points", "pts", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aPoints);	mser;

	aVelocities = tAttr.create("velocities", "vel", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aVelocities);	mser;


	aVectorSpeed =  nAttr.create( "vectorSpeed", "vsp", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aVectorSpeed);	mser;


	aEmissionVectors = tAttr.create("emissionVectors", "evec", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aEmissionVectors);	mser;


	///////////////////////////////
	aEmissionRadius =  nAttr.create( "sphereRadius", "shr", MFnNumericData::kDouble, 1.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aEmissionRadius);	mser;

	aEmissionRadii = tAttr.create("sphereRadiusPP", "shrpp", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aEmissionRadii);	mser;

	aEmissionRadiusBias =  nAttr.create( "sphereRadiusBias", "shrb", MFnNumericData::kDouble, 1.0 ); 
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st =	addAttribute(aEmissionRadiusBias);	mser;

	aEmissionRadiusBiasPP = tAttr.create("sphereRadiusBiasPP", "shrbpp", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aEmissionRadiusBiasPP);	mser;

	aEmissionShape = eAttr.create("emissionShape", "emsh");
	eAttr.addField("sphere", ppEmitter::kSphere);
	eAttr.addField("circle",  ppEmitter::kCircle);
	eAttr.setDefault(ppEmitter::kSphere);
	eAttr.setKeyable(true);
	eAttr.setWritable(true);
	st = addAttribute(aEmissionShape );mser;

	aEmissionShapeAxis = nAttr.create("emissionShapeAxis", "emsha", MFnNumericData::k3Double);mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault( 1.0, 0.0, 0.0 );

	st = addAttribute(aEmissionShapeAxis);	mser;

	aEmissionShapeAxisPP = tAttr.create("emissionShapeAxisPP", "emshapp", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aEmissionShapeAxisPP); mser;
	///////////////////////////////






	///////////////////////////////
	aParentIds = tAttr.create("parentIds", "pids", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aParentIds);	mser;
	///////////////////////////////

	///////////////////////////////
	aColors = tAttr.create("rgbPP", "rgbpp", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aColors);	mser;
	///////////////////////////////

	///////////////////////////////
	aRadius =  nAttr.create( "radius", "rad", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aRadius);	mser;

	aRadii = tAttr.create("radiusPP", "rdpp", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aRadii);	mser;

	aRadiusVariation =  nAttr.create( "radiusVariation", "rdv", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aRadiusVariation);	mser;
	///////////////////////////////


	///////////////////////////////
	aNormalSpeeds = tAttr.create("normalSpeedPP", "nsppp", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aNormalSpeeds);	mser;
	///////////////////////////////

	///////////////////////////////
	aSpreads = tAttr.create("spreadPP", "sprpp", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aSpreads);	mser;
	///////////////////////////////

	///////////////////////////////
	aSpeedRandoms = tAttr.create("speedRandomPP", "srpp", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aSpeedRandoms);	mser;
	///////////////////////////////

	aColorVariation = nAttr.createColor("colorVariation", "cvar");
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	st =	addAttribute(aColorVariation);	mser;

	aSweepShutterOpen = nAttr.create("sweepShutterOpen", "swso", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st =	addAttribute(aSweepShutterOpen);	mser;



	return( MS::kSuccess );
}

MStatus ppEmitter::compute(const MPlug& plug, MDataBlock& data)
{

	// JPMDBG;

	MStatus st;
	if( !(plug == mOutput) ) return( MS::kUnknownParameter );

	// has no effect
	if ( data.inputValue( state ).asShort() == 1 ) return MS::kSuccess;





	MObject thisNode = thisMObject();
	MFnDependencyNode thisNodeFn(thisNode);

	int multiIndex = plug.logicalIndex( &st);mser;
	// cerr << "multiIndex: " << multiIndex << endl;
	MArrayDataHandle hOutArray = data.outputArrayValue(mOutput, &st);mser;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st);mser;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;
	MFnArrayAttrsData fnOutput;
	MObject dOutput = fnOutput.create ( &st );mser;

	// position and velocity arrays to append new particle data.
	MVectorArray outPos = fnOutput.vectorArray("position", &st);mser;
	MVectorArray outVel = fnOutput.vectorArray("velocity", &st);mser;
	MDoubleArray outTime = fnOutput.doubleArray("timeInStep", &st);mser;
	MDoubleArray outParentRadius = fnOutput.doubleArray("parentRadius", &st);mser;
	MDoubleArray outParentParticleId = fnOutput.doubleArray("parentParticleId", &st);mser;
	MVectorArray outParentColor = fnOutput.vectorArray("parentColor", &st);mser;


	// JPMDBG;
	// check time and isFull
	bool beenFull = isFullValue( multiIndex, data );
	// JPMDBG;
	if( beenFull ) return( MS::kSuccess );
	// JPMDBG;
	MTime cT = currentTimeValue( data );
	MTime sT = startTimeValue( multiIndex, data );
	MTime dT = deltaTimeValue( multiIndex, data );
	double dt =  dT.as(MTime::kSeconds);

	// JPMDBG;

	if( (cT <= sT) || (dT <= 0.0) ){

		// JPMDBG;


		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}
	// JPMDBG;

	// bool validInput = true;
	MDataHandle h;
	MObject d;
	MFnVectorArrayData fnV;
	MFnDoubleArrayData fnD;

	MVector colorVariation = MVector(data.inputValue(aColorVariation).asFloatVector());
	double radiusVariation =  data.inputValue( aRadiusVariation ).asDouble() ;
	double vectorSpeed =  data.inputValue( aVectorSpeed ).asDouble() ;
	double sweepOpen =  data.inputValue( aSweepShutterOpen ).asDouble() ;


	double rate =  data.inputValue( mRate ).asDouble() ;

	h = data.inputValue( aPoints , &st); msert;
	d = h.data();
	st = fnV.setObject(d); msert;
	MVectorArray points = fnV.array( &st );msert;	
	unsigned pl = points.length();
	// cerr << "SIZE " << pl << endl;
	// JPMDBG;

	if ((!pl) || (rate <= 0.0 ))  {
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}

	// JPMDBG;

	MVectorArray velocities;
	h = data.inputValue( aVelocities , &st); mser;
	d = h.data();
	fnV.setObject(d); mser;
	if (fnV.array().length() == pl) {
		velocities.copy(fnV.array());
	} else {
		velocities = MVectorArray(pl);
	}

	// JPMDBG;
	
	MVectorArray emissionVectors(pl);
	h = data.inputValue( aEmissionVectors , &st); mser;
	d = h.data();
	fnV.setObject(d); mser;
	if (fnV.array().length() == pl) {
		const MVectorArray & evec = fnV.array();
		for (unsigned i = 0; i < pl; ++i)
		{
			emissionVectors[i] = evec[i] * vectorSpeed;
		}
	} else {
		emissionVectors = MVectorArray(pl);
	}
	// JPMDBG;

	MDoubleArray emissionRadii;
	st = multipliedArray(data,pl,aEmissionRadii,aEmissionRadius,emissionRadii);mser;
	
	MDoubleArray sphBiasVals;
	st = multipliedArray(data,pl,aEmissionRadiusBiasPP,aEmissionRadiusBias,sphBiasVals);mser;
	
	MDoubleArray parentIds;
	st = defaultedArray(data,pl,aParentIds,0.0,parentIds);mser;

	MVectorArray colors;
	st = defaultedArray(data, pl, aColors, MVector::zero, colors);mser;

	MDoubleArray radii;
	st = multipliedArray(data,pl,aRadii,aRadius,radii);mser;
	
	MObject  aNormalSpeed = thisNodeFn.attribute(MString("normalSpeed"));
	MDoubleArray normalSpeeds;
	st = multipliedArray(data,pl,aNormalSpeeds,aNormalSpeed,normalSpeeds);mser;

	MObject  aSpread = thisNodeFn.attribute(MString("spread"));
	MDoubleArray spreads;
	st = multipliedArray(data,pl,aSpreads,aSpread,spreads);mser;

	MObject  aSpeedRandom = thisNodeFn.attribute(MString("speedRandom"));
	MDoubleArray speedRandoms;
	st = multipliedArray(data,pl,aSpeedRandoms,aSpeedRandom,speedRandoms);mser;

	// JPMDBG;

	MObject  aRatePP = thisNodeFn.attribute(MString("ratePP"));
	MDoubleArray rates(pl);
	double rateVal = data.inputValue(mRate).asDouble();
	h = data.inputValue( aRatePP , &st); mser;
	d = h.data();
	fnD.setObject(d); mser;
	if (fnD.array().length() == pl) {
		const MDoubleArray & rpp = fnD.array();
		for (unsigned int i = 0; i < pl; ++i)
		{
			rates[i] = rpp[i] * rateVal;
		}
	} else {
		rates = MDoubleArray(pl, rateVal);
	}
	// JPMDBG;

	long seedVal = seedValue( multiIndex, data );
	double speed = doubleValue( data, mSpeed );

	srand48(seedVal);

	EmissionShape shape = (EmissionShape)data.inputValue(aEmissionShape).asShort();
	if (shape == ppEmitter::kSphere) {
	// JPMDBG;
		for (unsigned i = 0; i < pl; ++i)
		{

		//unsigned j = (i + 1);
			emitSphereFromPoint(
				dt, sweepOpen, rates[i], points[i], velocities[i], 
				emissionVectors[i], emissionRadii[i] , sphBiasVals[i], parentIds[i] , 
				colors[i] , radii[i] , normalSpeeds[i], spreads[i] , 
				speedRandoms[i], speed, colorVariation, radiusVariation, 
				outPos, outVel, outTime, outParentRadius, outParentParticleId, 
				outParentColor 
			);
		}
	} else if (shape == ppEmitter::kCircle) {

		MVector  axis = data.inputValue( aEmissionShapeAxis).asVector()  ;
		if (axis == MVector::zero) axis = MVector::xAxis;
		axis.normalize();
		MVectorArray emissionShapeAxisPP;
		
		st = defaultedArray(data, pl, aEmissionShapeAxisPP, axis, emissionShapeAxisPP);mser;

		for (unsigned i = 0; i < pl; ++i)
		{

			emitCircleFromPoint(
				dt, sweepOpen, rates[i], points[i], velocities[i], 
				emissionVectors[i], emissionShapeAxisPP[i], emissionRadii[i] ,
				sphBiasVals[i], parentIds[i] , colors[i] , radii[i] , 
				normalSpeeds[i], spreads[i] , speedRandoms[i], speed, 
				colorVariation, radiusVariation, outPos, outVel, outTime, 
				outParentRadius, outParentParticleId, outParentColor 
			);
		}
	}


	hOut.set( dOutput );
	data.setClean( plug );

	// JPMDBG;

	return( MS::kSuccess );
}


