
#include <maya/MIOStream.h>
#include <maya/MTime.h>
#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MPxEmitterNode.h>
#include "errorMacros.h"


class splashEmitter: public MPxEmitterNode
{
public:
	splashEmitter();
	virtual ~splashEmitter();



	static void		*creator();
	static MStatus	initialize();
	virtual MStatus	compute( const MPlug& plug, MDataBlock& data );


	// geometry samples output to field
	static MObject aSamplePoints;
	static MObject aSampleVelocities;
	static MObject aSampleMasses;
	static MObject aSampleDeltaTime;
	static MObject aSampleFieldData;
	// forces returned at the triangle centres
	static MObject aForces;

	static MObject aSweptGeometry;			///  triangles (will be an array)
	static MObject aInheritXVelocity;
	static MObject aInheritAvgVelocity;
	static MObject aInheritPressure;
	static MObject aSpeedThreshold;
	static MObject aAllowSelfIntersections;
	static MObject aMaxTimeStep;

	static  MObject         aBBMinX;
	static  MObject         aBBMinY;
	static  MObject         aBBMinZ;
	static  MObject         aBBMin;
	static  MObject         aBBMaxX;
	static  MObject         aBBMaxY;
	static  MObject         aBBMaxZ;
	static  MObject         aBBMax;
	static  MObject         aBBSizX;
	static  MObject         aBBSizY;
	static  MObject         aBBSizZ;
	static  MObject         aBBSiz;
	static  MObject         aBB;
	static  MObject         aUseBB;
	/*
	static  MObject         aMinPressure;
	static  MObject         aMaxPressure;
	*/
	//static MObject aSampleTriIndex;

	static  MObject         aVerbose;

	static MObject aFacingRemap;
	static MObject aPressureRemap;
	//static MObject aCompressionRemap;



	static MTypeId	id;


private:

	double  areaFn(const MVector &p1,const MVector &p2,const MVector &p3);
	/*
	unsigned int randomDistrib(long seedVal,double f, const MVector &p1,
	const MVector &p2,const MVector &p3,MVectorArray &result);

unsigned int randomDistrib(long seedVal,double f, const MPoint &p1,
const MPoint &p2,MVectorArray &result);
*/
void spreadVectors(long seedVal,const MVector & dir,double spread, MVectorArray & newVels);
void randSpeeds(long seedVal,double randomSpeed, MVectorArray & vels);


	//
long 	seedValue( int plugIndex, MDataBlock& data );
double	doubleValue( MDataBlock& data, MObject &att );
MVector	vectorValue( MDataBlock& data, MObject &att );
bool	isFullValue( int plugIndex, MDataBlock& data );
double	inheritFactorValue( int plugIndex, MDataBlock& data );
MTime	currentTimeValue( MDataBlock& data );
MTime	startTimeValue( int plugIndex, MDataBlock& data );
MTime	deltaTimeValue( int plugIndex, MDataBlock& data );

	// Force Accumulator procedure for gathering maya's force fields
MStatus getAppliedForces(
	MDataBlock& block,
	const MVectorArray &positions,
	const MVectorArray &velocities,
	const MDoubleArray &densities,
	const MTime &dT,
	MVectorArray &appliedForce
	);
};

// inlines
//
inline double splashEmitter::doubleValue( MDataBlock& data , MObject &att)
{
	MStatus status;

	MDataHandle hValue = data.inputValue( att, &status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


inline MVector splashEmitter::vectorValue( MDataBlock& data,MObject &att )
{
	MStatus status;
	MVector valueV(0.0, 0.0, 0.0);

	MDataHandle hValue = data.inputValue( att, &status );

	if( status == MS::kSuccess )
	{
		double3 &value = hValue.asDouble3();

		valueV[0] = value[0];
		valueV[1] = value[1];
		valueV[2] = value[2];
	}

	return(valueV );
}

inline bool splashEmitter::isFullValue( int plugIndex, MDataBlock& data )
{
	MStatus status;
	bool value = true;

	MArrayDataHandle mhValue = data.inputArrayValue( mIsFull, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asBool();
		}
	}

	return( value );
}

inline double splashEmitter::inheritFactorValue(int plugIndex,MDataBlock& data)
{
	MStatus status;
	double value = 0.0;

	MArrayDataHandle mhValue = data.inputArrayValue( mInheritFactor, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asDouble();
		}
	}

	return( value );
}

inline MTime splashEmitter::currentTimeValue( MDataBlock& data )
{
	MStatus status;

	MDataHandle hValue = data.inputValue( mCurrentTime, &status );

	MTime value(0.0);
	if( status == MS::kSuccess )
		value = hValue.asTime();

	return( value );
}

inline MTime splashEmitter::startTimeValue( int plugIndex, MDataBlock& data )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = data.inputArrayValue( mStartTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}

	return( value );
}

inline MTime splashEmitter::deltaTimeValue( int plugIndex, MDataBlock& data )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = data.inputArrayValue( mDeltaTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}
	return( value );
}

inline long splashEmitter::seedValue( int plugIndex, MDataBlock& data )
{
	MStatus status;
	long value = true;

	MArrayDataHandle mhValue = data.inputArrayValue( mSeed, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asInt();
			//cerr << value << endl;
		}
	}

	return( value );
}
