

#include <maya/MIOStream.h>
#include <math.h>
//#include <stdlib.h>


#include <maya/MDataHandle.h>
#include <maya/MPlugArray.h>
#include <maya/MRenderUtil.h> 
#include <maya/MQuaternion.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MArrayDataBuilder.h>

#include <maya/MDagPath.h>
#include <maya/MBoundingBox.h>
#include <maya/MFnMesh.h>

#include "meshVolumeEmitter.h"
#include "lookup.h"
#include "attrUtils.h"
#include "mayaMath.h"
#include "jMayaIds.h"


MObject meshVolumeEmitter::aInMesh;
MObject meshVolumeEmitter::aDivisions;


MObject meshVolumeEmitter::aSamplePoints;
MObject meshVolumeEmitter::aSampleVelocities;
MObject meshVolumeEmitter::aSampleMasses;
MObject meshVolumeEmitter::aSampleDeltaTime;
MObject meshVolumeEmitter::aSampleFieldData;
MObject meshVolumeEmitter::aForces;

MObject meshVolumeEmitter::aForceRate;
MObject meshVolumeEmitter::aInheritForce;

	
MTypeId meshVolumeEmitter::id( k_meshVolumeEmitter );

meshVolumeEmitter::meshVolumeEmitter(){}

meshVolumeEmitter::~meshVolumeEmitter(){}

void *meshVolumeEmitter::creator()
{
    return new meshVolumeEmitter;
}

MStatus meshVolumeEmitter::initialize()

{
   MStatus st;
 //  unsigned int counter = 0;
   MString method("meshVolumeEmitter::initialize");

	MFnTypedAttribute tAttr;
	MFnCompoundAttribute cAttr;
	MFnUnitAttribute uAttr;
	// MFnMessageAttribute mAttr;
	MFnNumericAttribute nAttr;


	aInMesh = tAttr.create( "inMesh", "inm", MFnData::kMesh);
	tAttr.setReadable(false);
	st =	addAttribute(aInMesh);	mser;

	aDivisions =  nAttr.create( "divisions", "dvs", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aDivisions);mser;
		
	aForceRate =  nAttr.create( "forceRate", "fcr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aForceRate);mser;

	aInheritForce =  nAttr.create( "inheritForce", "ifc", MFnNumericData::kDouble, 0.0);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
 	nAttr.setDefault(0.0);
 	st =	addAttribute(aInheritForce);	mser;
 	
	aSamplePoints = tAttr.create("samplePoints", "spts", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	
	aSampleVelocities = tAttr.create("sampleVelocities", "svls", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	
	aSampleMasses = tAttr.create("sampleMasses", "smss", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	
 	aSampleDeltaTime = uAttr.create( "sampleDeltaTime", "sdt", MFnUnitAttribute::kTime, 0.0);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	
	aSampleFieldData = cAttr.create("sampleFieldData","sfd");
	cAttr.addChild(aSamplePoints);
	cAttr.addChild(aSampleVelocities);
	cAttr.addChild(aSampleMasses);
	cAttr.addChild(aSampleDeltaTime);

	st = addAttribute(aSampleFieldData);mser;
	
	aForces = tAttr.create("forces", "frc", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setArray(true);
	st = addAttribute( aForces ); mser;

	attributeAffects(mCurrentTime,aSamplePoints);
	attributeAffects(mCurrentTime,aSampleVelocities);
	attributeAffects(mCurrentTime,aSampleMasses);
	attributeAffects(mCurrentTime,aSampleFieldData);
	
	return( MS::kSuccess );
}





// project rays up through the BB of the mesh from underneath.
// The rays are arranged on a 2D grid just underneath. 
// If the rays hit the mesh more than once then we have at least one span in which to generate points.
MStatus meshVolumeEmitter::gridPointsInMesh(
	const MObject &inMesh, 
	const double divs, 
	MFloatPointArray &pa,
	float & spacing,
	MBoundingBox &box) 
{
	
	MStatus st;	

	MFnMesh fnM(inMesh, &st);msert;
	
	unsigned n = fnM.numVertices();
	MFloatPointArray pts;
	st = fnM.getPoints (pts) ;
	// MBoundingBox box;
	for(unsigned i = 0; i < n; ++i)	box.expand(pts[i]);


	MMeshIsectAccelParams ap = fnM.autoUniformGridParams();

	
	// generate points in XZ at the top of the BB pointing down
	double xSize = box.width();
	double ySize = box.height();
	double zSize = box.depth();
	if ((xSize <= 0) || (ySize <= 0) || (zSize <= 0) ) return MStatus::kFailure;

	
	
	double maxAxis = (xSize > ySize) ? xSize : ySize;
	maxAxis = (maxAxis > zSize) ? maxAxis : zSize;
	spacing = float(maxAxis / divs);
	
	unsigned nx = unsigned(xSize/spacing) + 1;
	unsigned ny = unsigned(ySize/spacing) + 1;
	unsigned nz = unsigned(zSize/spacing) + 1;

  //  
	double xOffset =( (xSize - ((nx-1) * spacing )) / 2.0);
	double yOffset =( (ySize - ((ny-1) * spacing )) / 2.0);
	double zOffset =( (zSize - ((nz-1) * spacing )) / 2.0);

	// to be safe pick a place underneath the min y to shoot rays from
	float under = float(box.min().y - (spacing / 2.0)) ; 
	MFloatPoint fpSource;
	MFloatVector fvRayDir(0.0f, 1.0f, 0.0f);
	
	float maxParam = float(box.height() + spacing);
	MFloatPointArray hitPoints;

	float firstY = float(box.min().y + yOffset);	
//	cerr << " firstY "  << firstY <<  endl;
	
	float currY ;	
	float currX = float(box.min().x + xOffset);	
	for (unsigned x = 0;x<nx;x++) {
		float currZ = float(box.min().z + zOffset);
		for (unsigned z = 0;z<nz;z++) {
			fpSource = MFloatPoint(currX, under, currZ);
			bool hit = fnM.allIntersections(fpSource, fvRayDir, NULL, NULL, false, MSpace::kWorld, maxParam,false, 	&ap, true, hitPoints, NULL, NULL, NULL, NULL, NULL, 0.000001f, &st);
			unsigned spans =0;
			if (hit) spans = hitPoints.length() / 2; // if odd num hitPoints we ignore the last and assume the valid spans are the first
			for (unsigned s = 0;s<spans;s++) {
				const MFloatPoint &hitStart = hitPoints[(s*2)];
				const MFloatPoint &hitEnd = hitPoints[((s*2)+1)];
				// round up to the next grid position
				currY = float( ((ceil( (hitStart.y - firstY) / spacing)) * spacing) + firstY); 
				// cerr << " curr "  << currX << " " << currY << " " << currZ <<  endl;

				while (currY < hitEnd.y) {
					pa.append(MFloatPoint(currX,currY,currZ));
					currY += spacing;
				}
			} 
			currZ += spacing;
		}
		currX += spacing;		
	}	
	return( MS::kSuccess );
}

// Force Accumulator procedure for gathering maya's force fields
MStatus meshVolumeEmitter::getAppliedForces(
	 MDataBlock& block,
	 const MVectorArray &positions,
	 const MVectorArray &velocities,
	 const MDoubleArray &masses,
	 const MTime &dT,
	 MVectorArray &appliedForce
	 )
{
	
	MStatus st;
	// dont do anything if no points
	unsigned siz = positions.length();
	if (siz==0){return MS::kUnknownParameter;}
	
	// dont do anything if no forces
	MPlug forcesPlug(thisMObject(), aForces);  //  force plug
	unsigned numForces = forcesPlug.numElements(&st);if (st.error()){return st;}
	if (! numForces) {return MS::kUnknownParameter;}
	
	// note - we haven't checked the length of arrays, and indeed the masses (densities) array
	// will be empty - lets see if the forces complain -hopefully they will assume a value of 1.
	
	//unsigned int counter = 0;
	appliedForce = MVectorArray(siz, MVector::zero);
	MDataHandle hSampleFieldData = block.outputValue(aSampleFieldData, &st );if (st.error()){return st;}
	MDataHandle hSamplePoints = hSampleFieldData.child(aSamplePoints);
	MDataHandle hSampleVelocities = hSampleFieldData.child(aSampleVelocities );
	MDataHandle hSampleMasses = hSampleFieldData.child(aSampleMasses );
	MDataHandle hDeltaTime = hSampleFieldData.child(aSampleDeltaTime );

	MFnVectorArrayData fnSamplePoints;
	MObject dSamplePoints = fnSamplePoints.create( positions, &st ); ;if (st.error()){return st;}
	MFnVectorArrayData fnSampleVelocities;
	MObject dSampleVelocities = fnSampleVelocities.create( velocities, &st ); ;if (st.error()){return st;}
	MFnDoubleArrayData fnSampleMasses;
	MObject dSampleMasses = fnSampleMasses.create( masses, &st ); ;if (st.error()){return st;}

	hSamplePoints.set(dSamplePoints);
	hSampleVelocities.set(dSampleVelocities);
	hSampleMasses.set(dSampleMasses);
	hDeltaTime.set(dT);

	block.setClean(aSamplePoints);
	block.setClean(aSampleVelocities);
	block.setClean(aSampleMasses);
	block.setClean(aSampleDeltaTime);
	block.setClean(aSampleFieldData);

	unsigned found = false;
	for (unsigned nf = 0;nf<numForces;nf++){
		MPlug tmpForcePlug = forcesPlug.elementByPhysicalIndex(nf,&st);if (st.error()){continue;}
		if (tmpForcePlug.isConnected()) {
			MObject tmpForceObject;
			st = tmpForcePlug.getValue(tmpForceObject); if (st.error()){continue;}
			MFnVectorArrayData tmpForceFn(tmpForceObject);
			MVectorArray tmpForce = tmpForceFn.array(&st);if (st.error()){continue;}
			if (tmpForce.length() == siz) {
				found =true;
				for (unsigned t=0; t < siz; t++ ) {
					appliedForce[t] += tmpForce[t];
				}
			}
		}
	}
	
	if (!found) return MS::kUnknownParameter;
	return MS::kSuccess;
}


bool meshVolumeEmitter::sampleTexRate(MFloatPointArray &pa, MFloatArray &densities) {
	MStatus st;
	MPlugArray plugArray;	
	MFnDependencyNode thisNodeFn(thisMObject());
	MObject  aTextureRate = thisNodeFn.attribute(MString("textureRate"));
	MPlug texRatePlug(thisMObject(), aTextureRate);
	bool useTexRate = texRatePlug.connectedTo(plugArray,1,0,&st); mser;
	if (st.error()) return false;
	
	unsigned len = pa.length();
	MFloatVectorArray resTransparencies;
	
	MString name; 
	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();
	name = plugArray[0].name(&st); mser;
	MFloatVectorArray  resColors;
	st =  MRenderUtil::sampleShadingNetwork (
		name,			   	// shadingNodeName Name of the shading node/shading engine 
		len,	   			// numSamples Number of samples to be calculated 
		false,  		   	// useShadowMaps Whether to calculate shadows 
		false,  		   	// reuseMaps If calculating shadows, whether to reuse shadowmaps 
		cameraMat,		   	// cameraMatrix The eyeToWorld matrix to be used for conversion 
		&pa,				// points Locations to be sampled in world space 
		NULL,		   		// uCoords U coordinates of the samples 
		NULL,		   		// vCoords V coordinates of the samples 
		NULL,				// normals Normals at the sample points in world space 
		&pa,				// refPoints RefPoints to be used for 3D texture in world space 
		NULL, 			   	// tangentUs U tangents at the sample points in world space 
		NULL,			   	// tangentVs V tangents at the sample points in world space 
		NULL, 			   	// filterSizes Filter sizes to be used for 2D/3D textures 
		resColors,		   		// resultColors Storage for result colors 
		resTransparencies  	// resultTransparencies storage for result transparencies	
	);

	if (st.error()) return false;
	densities.setLength(len);
	for (unsigned i=0;i<len;i++) {
		densities[i] = resColors[i].x;
	}
	
	return true;
}


MStatus meshVolumeEmitter::compute(const MPlug& plug, MDataBlock& data)
{
 	MStatus st;
	if( !(plug == mOutput) ) return( MS::kUnknownParameter );

	
//	cerr << " here AAA  "  <<  endl;		
	
	///////////////////////////////////////////////////////
	int multiIndex = plug.logicalIndex( &st);mser;
	MArrayDataHandle hOutArray = data.outputArrayValue(mOutput, &st);mser;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st);mser;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;
	MFnArrayAttrsData fnOutput;
	MObject dOutput = fnOutput.create ( &st );mser;
	///////////////////////////////////////////////////////

//	cerr << " here BBB  "  <<  endl;		

	///////////////////////////////////////////////////////
	MVectorArray outPos = fnOutput.vectorArray("position", &st);mser;
	MVectorArray outVel = fnOutput.vectorArray("velocity", &st);mser;
	MDoubleArray outTime = fnOutput.doubleArray("timeInStep", &st);mser;
	//outPos.clear();
	//outVel.clear();
	//outTime.clear();
	
	///////////////////////////////////////////////////////

//	cerr << " here CCC  "  <<  endl;		

	///////////////////////////////////////////////////////
	bool beenFull = isFullValue( multiIndex, data );
	if( beenFull ) return( MS::kSuccess );
	long seedVal = seedValue( multiIndex, data );
	srand48(seedVal);
	///////////////////////////////////////////////////////
	
//	cerr << " here DDD  "  <<  endl;		
	
	///////////////////////////////////////////////////////
	MTime cT = data.inputValue( mCurrentTime ).asTime();
	MTime sT = startTimeValue( multiIndex, data );
	MTime dT = deltaTimeValue( multiIndex, data );
	double dt = dT.as( MTime::kSeconds );
	if( (cT <= sT) || (dT <= 0.0) )
	{
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}
	///////////////////////////////////////////////////////
//	cerr << " here EEE  "  <<  endl;		


	///////////////////////////////////////////////////////
	MFnDependencyNode thisNodeFn(thisMObject());
	MObject  aSpread = thisNodeFn.attribute(MString("spread"));
	// MObject  aNormalSpeed = thisNodeFn.attribute(MString("normalSpeed"));
	MObject  aSpeedRandom = thisNodeFn.attribute(MString("speedRandom"));
	MObject  aScaleRate = thisNodeFn.attribute(MString("scaleRateByObjectSize"));
	MObject  aEnableTextureRate = thisNodeFn.attribute(MString("enableTextureRate"));
	double speed = data.inputValue( mSpeed ).asDouble();
	double speedRandom = data.inputValue( aSpeedRandom ).asDouble();
	// double normalSpeed = data.inputValue( aNormalSpeed ).asDouble();
	double spread = data.inputValue( aSpread ).asDouble();
	bool scaleRate = data.inputValue( aScaleRate ).asBool();
	double rate = data.inputValue( mRate ).asDouble(); 

	
	double inheritForce = data.inputValue( aInheritForce ).asDouble();
	double forceRate = data.inputValue( aForceRate ).asDouble();
	double3 &dirV3 = data.inputValue( mDirection ).asDouble3();
	MVector dirV(dirV3[0], dirV3[1],dirV3[2]);	
	if (spread > 1 ) spread = 1;
	if (spread < 0 ) spread = 0;
	///////////////////////////////////////////////////////

	if (rate <= 0.0)
	{
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}



	///////////////////////////////////////////////////////
	MObject inMesh =  data.inputValue(aInMesh).asMeshTransformed();
	
	double divs = data.inputValue( aDivisions ).asDouble();
	if (divs == 0.0 ) divs = 1.0;
	MFloatPointArray pa; 
	// cerr << "going to gridPointsInMesh "  << endl;
	float spacing;
	MBoundingBox box;
	st = gridPointsInMesh(inMesh, divs ,pa, spacing,box);msert;
	unsigned len = pa.length();
	
	if (! len)	{
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}
	
	///////////////////////////////////////////////////////
//	cerr << " here FFF  "  <<  endl;		



//	cerr << " here GGG  "  <<  endl;		


	///////////////////////////////////////////////////////
	bool useTexRate = data.inputValue(aEnableTextureRate).asBool(); 
	MFloatArray texDensities;
	if (useTexRate) 	useTexRate = sampleTexRate(pa,texDensities);
	///////////////////////////////////////////////////////
//	cerr << "len  "  << len <<  endl;		


	///////////////////////////////////////////////////////
	MVectorArray appliedForce;
    MDoubleArray mas(len,1.0); 
    MVectorArray vel(len); 
    MVectorArray pos(len); 

	for(unsigned i = 0; i < len; ++i) pos[i] = MVector(pa[i]);
    st = getAppliedForces(data,pos ,vel ,mas ,dT,appliedForce);
	bool useForces = (! st.error());
	///////////////////////////////////////////////////////
	
//	cerr << " here JJJJ  "  <<  endl;		
	
//	cerr << " aboyut to loop  "  <<  endl;		


	double rateNormalize = 1.0 /  (divs * divs * divs);
	
	double boxX = box.width();
	double boxY = box.height();
	double boxZ = box.depth();
	
	if (scaleRate) rateNormalize = rateNormalize * (boxX * boxY * boxZ) * 0.00001;
	
	for(unsigned i = 0; i < len; ++i)
	{
		
		///////////////////////////////////////////////////////
		double thisRate = rate * rateNormalize; 
		MVector force ;
		if (useForces) {
			force= appliedForce[i];
			thisRate = thisRate * force.length() * forceRate;
		}
		if (useTexRate) thisRate = thisRate * texDensities[i];
		unsigned int nPoints = int(thisRate);
		nPoints += drand48() < (thisRate - double(nPoints)) ? 1 : 0;
		if (nPoints <= 0) continue;
		///////////////////////////////////////////////////////
 	//	cerr << " nPoints  "  << nPoints  <<  endl;		
		
		
		///////////////////////////////////////////////////////
		MVectorArray velocities;
		
		MVector inheritVec(MVector::zero);		
		if (inheritForce) inheritVec = (inheritForce * force ) ;

		MVector newVec(dirV);

		if (!speedRandom) newVec *= speed;
		
		if (spread) {
			mayaMath::sphericalSpread(newVec,spread, nPoints, velocities);
		} else {
			for(unsigned j=0;j < nPoints;j++){
				velocities.append(newVec);
			}
		}
	
		for(unsigned j=0;j < nPoints;j++){
			velocities.set(velocities[j] + inheritVec ,j);
		}
		
		if (speedRandom) mayaMath::randomizeSpeeds(speedRandom,speed, velocities);
		///////////////////////////////////////////////////////

//		cerr << " here  "  <<  endl;		

		///////////////////////////////////////////////////////
		for(unsigned j=0;j < nPoints;j++){
			MVector randPos =  (MVector(drand48(),drand48(),drand48()) - MVector(0.5,0.5,0.5))  * spacing;
			randPos += MVector(pa[i]);
			outPos.append(randPos);
			outVel.append(velocities[j]);
			outTime.append(0.5);
		}
		///////////////////////////////////////////////////////
	//	cerr << " here 2  "  <<  endl;		

	}	
	
	hOut.set( dOutput );
	data.setClean( plug );
	return( MS::kSuccess );
	
}


