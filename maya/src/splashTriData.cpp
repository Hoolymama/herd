// Copyright (C) 2001 hoolyMama 
//
// Author: Julian Mann
//
#define P_TOLERANCE 0.000001
#include "splashTriData.h"


// const double overlapTolerance = 0.01;

splashTriData::splashTriData() // another constructor 
	//:_splashTriBoxVector(0),
	:_perm(0),
	_pRoot(0),
	_maxPointsPerBucket(2)
{
	_pRoot = new splashTriBoxKdNode;
	_perm = new VECTOR_OF_TRIBOX_POINTERS;

}
splashTriData::~splashTriData()
{
	if (_perm) {
		VECTOR_OF_TRIBOX_POINTERS::iterator p =  _perm->begin();
		while (p != _perm->end()) {
			delete *p;
			*p =0;
			p++;
		}
		delete _perm;
		_perm = 0;
	}
	makeEmpty();  // recursively delete tree except for root
	//if ( _pRoot) {
	delete _pRoot;  // then delete root
	_pRoot = 0;
	//}
	
}
const splashTriBoxKdNode * splashTriData::root(){
	return _pRoot;
};

MStatus splashTriData::addTriBoxes(const MFnDynSweptGeometryData  &g1, unsigned el)  {
 	MStatus st;
	//unsigned int counter = 0;
	MString method("splashTriData::addTriBoxes");
	
	unsigned int tl1 = g1.triangleCount(&st);mser;

	if (tl1) {
		unsigned int  i ;
		for ( i = 0; i < tl1; i++) {
			MDynSweptTriangle t1 =  g1.sweptTriangle(i);
			splashTriBox * t = new splashTriBox(t1, el);	
			_perm->push_back(t) ;
		}
	} else {
		return MS::kFailure;
	}
	//cerr <<"out addTriBoxes " << endl;
	return MS::kSuccess;
}




MStatus splashTriData::addTriBoxes(const MFnDynSweptGeometryData  &g1, unsigned el, const MBoundingBox &box)   {
 	MStatus st;
	//unsigned int counter = 0;
	MString method("splashTriData::addTriBoxes");
	
	unsigned int tl1 = g1.triangleCount(&st);mser;
	
	if (tl1) {
		unsigned int  i ;
		for ( i = 0; i < tl1; i++) {
			MDynSweptTriangle t1 =  g1.sweptTriangle(i);
			if (! box.contains(MPoint(t1.vertex(0)))) continue;
			if (! box.contains(MPoint(t1.vertex(1)))) continue;
			if (! box.contains(MPoint(t1.vertex(2)))) continue;
			splashTriBox * t = new splashTriBox(t1, el);	
			_perm->push_back(t) ;
		}
	} else {
		return MS::kFailure;
	}
	//cerr <<"out addTriBoxes " << endl;
	return MS::kSuccess;
}

int splashTriData::size() {
	return int(_perm->size());
}

void splashTriData::build(){
	int low = 0;
	int high = (size() -1);
	_pRoot = build(low, high);

	VECTOR_OF_TRIBOX_POINTERS::iterator currentBox = _perm->begin();
	while(currentBox != _perm->end()){
		setOverlapList(_pRoot , *currentBox);
		currentBox++;
	}
}


splashTriBoxKdNode *  splashTriData::build(	 int low,  int high	){
	// // cout << "in subBuild routine " << endl;
	splashTriBoxKdNode * p = new splashTriBoxKdNode;
	if ((high < low) ) {
		p->empty = true;
	}  else {
		p->empty = false;
	}
 	if (((high - low) + 1) <= int(_maxPointsPerBucket)) {
		// only bucket nodes will hold a overlapList
		p->bucket = true;
		p->loPoint = low;
		p->hiPoint = high;
		p->loChild = 0;
		p->hiChild = 0;
	} else {
		
		p->bucket = false;
		p->cutAxis = findMaxAxis(low, high);
		 int mid = ((low + high) / 2);
		wirthSelect(low, high, mid, p->cutAxis);
		// quickPartition(low, high, mid, p->cutAxis);
		p->cutVal = ((*_perm)[mid])->center(p->cutAxis);
		p->loChild = build(low, mid);
		p->hiChild = build(mid+1, high);
	}
	return p;
}


void splashTriData::makeEmpty() {
	 
	if (_pRoot != 0) {	
		
		if (!(_pRoot->bucket)) {
			makeEmpty(_pRoot->loChild);	
			makeEmpty(_pRoot->hiChild);

		}
		_pRoot->loChild = 0;
		_pRoot->hiChild = 0;
	}
	// cout << "leaving makeEmpty" << endl;

}

void splashTriData::makeEmpty(splashTriBoxKdNode * p) {
	if (p) { // if the pointer aint NULL
		if (!(p->bucket)) {
			makeEmpty(p->loChild);	// recurse through kids
			makeEmpty(p->hiChild);
		}
		delete p;
		p = 0;					// zap the little bugger
	}
}
void splashTriData::wirthSelect(  int left,  int right,  int k, axis cutAxis )  
{
	int n = (right - left) + 1;
	if (n <= 1) return;
	int i,j,l,m;
	splashTriBox *x;
	splashTriBox *tmp;
	
	l=left;
	m=right;
	while (l<m) {
		x = (*_perm)[k];
		i=l;
		j=m;
		do {
			while (  ((*_perm)[i])->center(cutAxis) <  x->center(cutAxis)  ) i++;
			while (  ((*_perm)[j])->center(cutAxis) >  x->center(cutAxis)  ) j--;
			
			if (i<=j) {
				// swap
				tmp = (*_perm)[i];  
				(*_perm)[i] =(*_perm)[j] ;  
				(*_perm)[j] =tmp ;
				i++; j--;
			}
		} while(i<=j);
		if (j<k) l=i;
		if (k<i) m=j;
	}
}
		
				
void splashTriData::sortBoxPtrs ( VECTOR_OF_TRIBOX_POINTERS * p){
	int c = int(p->size());
	if ( c  > 2){
		sortBoxPtrs(0, (c - 1), p );
	}
}
				
					
void splashTriData::sortBoxPtrs ( int left,  int right, VECTOR_OF_TRIBOX_POINTERS * p)
{
	splashTriBox* pivot;
	splashTriBox* temp;
	pivot = (*p)[ ( left + right ) / 2 ];
	int splitL = left;   
	int splitR = right; 
 	do {    
		while ((*p)[splitL]<pivot) splitL++; 
		while ((*p)[splitR]>pivot) splitR--;
		if (splitL<=splitR)
		{
		    temp=(*p)[splitL]; 
			(*p)[splitL]=(*p)[splitR]; 
			(*p)[splitR]=temp;
		     splitL++; 
		     splitR--; 
		}
	} while ( splitL <= splitR );

	if (left<splitR) sortBoxPtrs(left, splitR,p);
	if (splitL<right) sortBoxPtrs(splitL, right,p);
}
	

axis splashTriData::findMaxAxis(const  int low, const  int high) const {
	 
	
	// The idea here is just to find the axis containing the longest 
	// side of the bounding rectangle of the points

	// From a vector of N points we just take sqrtN samples
	// in order to keep the time down to O(N)
	// should be ok though
	double minx , miny,  minz , maxx , maxy , maxz, tmpVal;
	double sx, sy, sz;
	
	 int  num = (high - low ) +1;
	 int interval = int(sqrt(double(num)));
	// unsigned int intervalSq = interval*interval;
	int i;
	MPoint p = ((*_perm)[low])->center();
	minx = p.x;
	maxx = minx;
	miny = p.y;
	maxy = miny;
	minz = p.z;
	maxz = minz;
	
	for (i= (low + interval); i<=high;i+=interval ) {
		p = ((*_perm)[i])->center();
		tmpVal= p.x;
		if (tmpVal < minx) {
			minx = tmpVal;
		} else {
			if (tmpVal > maxx) {
				maxx = tmpVal;
			}
		}
		tmpVal= p.y;
		if (tmpVal < miny) {
			miny = tmpVal;
		} else {
			if (tmpVal > maxy) {
				maxy = tmpVal;
			}
		}
		tmpVal= p.z;
		if (tmpVal < minz) {
			minz = tmpVal;
		} else {
			if (tmpVal > maxz) {
				maxz = tmpVal;
			}
		}
	}
	sx = maxx - minx;
	sy = maxy - miny;
	sz = maxz - minz;

	if (sx > sy) {
		// y is not the greatest
		if (sx > sz) {
			return mayaMath::xAxis;
		} else {
			return mayaMath::zAxis;
		}
	} else {
		// x is not the greatest
		if (sy > sz) {
			return mayaMath::yAxis;
		} else {
			return mayaMath::zAxis;
		}
	}
}

void  splashTriData::setOverlapList(splashTriBoxKdNode * p,  splashTriBox * tb  )  {
	// recursive setoverlapList method
	if (p->bucket) {
		p->overlapList.push_back(tb);
	} else {
		if (tb->min(p->cutAxis) < p->cutVal) {
			setOverlapList(p->loChild, tb);
		}
		if (tb->max(p->cutAxis) > p->cutVal) {
			setOverlapList(p->hiChild, tb);
		}
	}
}



void  splashTriData::boxSearch(const splashTriBoxKdNode * p, splashTriBox * searchBox, VECTOR_OF_TRIBOX_POINTERS * resultBoxes)  {
	if (p->bucket) {
		makeResultBoxes( &(p->overlapList), searchBox,resultBoxes) ;
	} else {
		if (searchBox->box().min()[(p->cutAxis)]  < p->cutVal) {
			boxSearch(p->loChild,searchBox,resultBoxes);
		}
		if (searchBox->box().max()[(p->cutAxis)] > p->cutVal) {
			boxSearch(p->hiChild,searchBox,resultBoxes);
		}
	}
}

void  splashTriData::makeResultBoxes(const VECTOR_OF_TRIBOX_POINTERS * overlapList,  splashTriBox *searchBox, VECTOR_OF_TRIBOX_POINTERS *resultBoxes)const  {
	VECTOR_OF_TRIBOX_POINTERS::const_iterator currentTriBox;
	currentTriBox = overlapList->begin();

	while(currentTriBox != overlapList->end()){  
		if (searchBox != *currentTriBox) { // if it's not me
			if (searchBox->boxIntersects(*currentTriBox)) {
				if (!((*currentTriBox)->met(searchBox))){ // if we havn't already tested against this triangle
					resultBoxes->push_back(*currentTriBox);
					searchBox->registerVisit(*currentTriBox);
				}
			}
		}
		currentTriBox++;
	}
}


VECTOR_OF_TRIBOX_POINTERS & splashTriData::triangleList(){
	return *_perm;
}

