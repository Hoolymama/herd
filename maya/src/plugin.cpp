/*
 *  plugin.cpp
 *  jtools
 *
 *  Created by Julian Mann on 27/11/2006.
 *  Copyright 2006 hoolyMama. All rights reserved.
 *
 */


#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include "errorMacros.h"


#include "ppEmitter.h"
#include "fieldEmitter.h"
#include "curveEmitter.h"
#include "meshVolumeEmitter.h"

#include "splashEmitter.h"
#include "transformEmitter.h"





MStatus initializePlugin( MObject obj )
{
	
	MStatus st;
	
	MString method("initializePlugin");

	MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);

	st = plugin.registerNode( "ppEmitter", ppEmitter::id, ppEmitter::creator, ppEmitter::initialize, MPxNode::kEmitterNode  );mser;
	st = plugin.registerNode( "fieldEmitter", fieldEmitter::id, fieldEmitter::creator, fieldEmitter::initialize, MPxNode::kEmitterNode  );mser;
	st = plugin.registerNode( "curveEmitter", curveEmitter::id, curveEmitter::creator, curveEmitter::initialize, MPxNode::kEmitterNode  );mser;
	st = plugin.registerNode( "meshVolumeEmitter", meshVolumeEmitter::id, meshVolumeEmitter::creator, meshVolumeEmitter::initialize, MPxNode::kEmitterNode  );mser;
	st = plugin.registerNode( "splashEmitter", splashEmitter::id, splashEmitter::creator, splashEmitter::initialize, MPxNode::kEmitterNode  );mser;
	st = plugin.registerNode( "transformEmitter", transformEmitter::id, transformEmitter::creator, transformEmitter::initialize, MPxNode::kEmitterNode  );mser;

	MGlobal::executePythonCommandOnIdle("from herd import menu;menu.HerdMenu()", true);

	return st;
	
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus st;
	
	MString method("uninitializePlugin");
	
	MFnPlugin plugin( obj );
	st = plugin.deregisterNode( transformEmitter::id );mser;
	st = plugin.deregisterNode( splashEmitter::id );mser;
	st = plugin.deregisterNode( meshVolumeEmitter::id );mser;

	st = plugin.deregisterNode( curveEmitter::id );mser;
	st = plugin.deregisterNode( fieldEmitter::id );mser;

	st = plugin.deregisterNode( ppEmitter::id );mser;

	
	return st;
}


