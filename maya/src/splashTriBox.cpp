#include <maya/MString.h>
#include "splashTriBox.h"

const double TIMESTEP = 1.0f / 24.0f;
const double EPSILON = 0.0001;
const double NEG_EPSILON = -0.0001;

splashTriBox::splashTriBox(){}

splashTriBox::splashTriBox( MDynSweptTriangle &t, unsigned parentId):_v(3), m_id(parentId){
	//cerr << "in splashTriBox constructor"<<endl;
	_v.set(MPoint(t.vertex(0,1.0)),0);
	_v.set(MPoint(t.vertex(1,1.0)),1);
	_v.set(MPoint(t.vertex(2,1.0)),2);
		
		
	
	MPoint o0 = MPoint(t.vertex(0,0.0));
	MPoint o1 = MPoint(t.vertex(1,0.0));
	MPoint o2 = MPoint(t.vertex(2,0.0));

	_change = ( (_v[0] - o0) + (_v[1] - o1) + (_v[2] - o2)) / 3.0;
	//_velocity = change / TIMESTEP;
	//_velocity = change;
	//cerr <<" _velocity " << _velocity << endl;
	_normal	=t.normal();
	computeBoundingBox();
	//cerr << "out splashTriBox constructor"<<endl;
}

splashTriBox::~splashTriBox(){}

splashTriBox& splashTriBox::operator=(const splashTriBox& other) {
	if (this != &other) {
		_v[0] = other._v[0];
		_v[1] = other._v[1];
		_v[2] = other._v[2];
		_box = other._box;
		_change	= other._change;
	}
	return *this;
}

void splashTriBox::computeBoundingBox(){
	//cerr << "in computeBoundingBox"<<endl;
	_box = MBoundingBox(_v[0],_v[1]);
	_box.expand(_v[2]);
	//cerr << "out computeBoundingBox"<<endl;
	_center = _box.center();
}
/*
void splashTriBox::makeLocator(const MPoint &p){
	// for debug				
	MString cmd;
	cmd = "spaceLocator -p 0 0 0;move -a ";
	cmd +=p.x;
	cmd +=" ";
	cmd +=p.y;
	cmd +=" ";
	cmd +=p.z;
	cmd +=";";
	MGlobal::executeCommand(cmd);
}
void splashTriBox::makeLocator(const MVector &p){
	// for debug				
	MString cmd;
	cmd = "spaceLocator -p 0 0 0;move -a ";
	cmd +=p.x;
	cmd +=" ";
	cmd +=p.y;
	cmd +=" ";
	cmd +=p.z;
	cmd +=";";
	MGlobal::executeCommand(cmd);

}

void splashTriBox::makeLineSegment(const MPoint &start, const MPoint &end){

	// for debug				
	MString command("curve -d 1 -p ");
	command += start.x;
	command += " ";
	command += start.y;
	command += " ";
	command += start.z;
	command += " -p ";
	command += end.x;
	command += " ";
	command += end.y;
	command += " ";
	command += end.z;
	command += ";";
	MGlobal::executeCommand(command);

}


void splashTriBox::makeBox(){
	// for debug
	double w = _box.width();
	double h = _box.height();
	double d = _box.depth();
	MPoint c = _box.center();
	MString command( "polyCube -w " );
	command += w ;
	command += " -h " ;
	command += h  ;
	command += " -d " ;
	command += d  ;
	command += " -sx 1 -sy 1 -sz 1 -ax 0 1 0 -tx 1 -ch 0;" ;
	command += "move -a " ;
	command += c.x;
	command += " ";
	command += c.y;
	command += " ";
	command += c.z;
	command += ";";
	MGlobal::executeCommand(command);
}
*/
const unsigned & splashTriBox::id() const {
	return m_id;
}
const MPoint & splashTriBox::center() const {
	return _center;
}
const double  splashTriBox::center(axis a) const {
	return  _center[a];
}
const double  splashTriBox::min(axis a) const {
	return _box.min()[a];
}
const double  splashTriBox::max(axis a) const {
	return _box.max()[a];
}

const MPoint & splashTriBox::vertex(int i) const {
	return _v[i];
}
const MVector & splashTriBox::normal() const {
	return _normal;
}
const MVector & splashTriBox::change() const {
	return _change;
}

const MBoundingBox & splashTriBox::box() const {
	return _box;
}

bool splashTriBox::boxIntersects(const splashTriBox  *other){

	if (other->box().max()[0] <= _box.min()[0]) return false;
	if (other->box().min()[0] >= _box.max()[0]) return false;

	if (other->box().max()[1] <= _box.min()[1]) return false;
	if (other->box().min()[1] >= _box.max()[1]) return false;
	
	if (other->box().max()[2] <= _box.min()[2]) return false;
	if (other->box().min()[2] >= _box.max()[2]) return false;

	return  true;
}

void splashTriBox::registerVisit( splashTriBox * other){
	_visited.push_back(other);
}


bool splashTriBox::met(const splashTriBox *other){
	//cerr << "in met"<<endl;
	if (_visited.size()) {
		VECTOR_OF_TRIBOX_POINTERS::iterator currentTriBox;
		currentTriBox = _visited.begin();
		while(currentTriBox != _visited.end()){  
			//cerr << "testing " << other << " against "<< (*currentTriBox) << endl;
			if (other == (*currentTriBox)) {
				return true;
			}
			currentTriBox++;
		}
	}
	return false;
}


bool splashTriBox::isMoving(){
	if (fabs(_change.x) > EPSILON) return true;
	if (fabs(_change.y) > EPSILON) return true;
	if (fabs(_change.z) > EPSILON) return true;
	return false;
}


inline short splashTriBox::sign(double v) {
	//cerr << "in sign"<<endl;
	
	if (v < -(EPSILON) ) return -1;
	if (v > EPSILON) return 1;
	//cerr << "out sign"<<endl;
	return 0;
}



bool splashTriBox::lineIntersectsPlane(
	const MVector &n,
	const MPoint &p,
	const MPoint &a,
	const MPoint &b, 
	MPoint &r
){
	MVector v = (b-a);
	double denominator = n*v;
	if (fabs(denominator) < EPSILON) {
		return false;
	}
	double numerator = n*(p-a);
	double u = numerator/denominator;
	if ((u<0.0) || (u>1.0)){
		return false;
	}
	r = a + (u*v);
	return true;
}






inline bool splashTriBox::inRange(const double& v) {
	if (v < 0.0) return false;
	if (v > 1.0) return false;
	return true;
}

int splashTriBox::getRegion(const MVector &bary)
{
/*	
				4
			   \ |
				\|
				 Y
				 |\
			   5 | \  3
				 |  \
				 | 0 \
			 ----X----Z---
			   6 |  1  \  2
				 |  	\
*/	
	bool x = (bary.x > EPSILON);
	bool y = (bary.y > EPSILON);
	bool z = (bary.z > EPSILON);

	if (x && y && z) return 0; 

	if ((!x) && y && z) return 3;
	if (x && (!y) && z) return  1;
	if (x && y && (!z)) return  5;
	
	if ((!x) && (!y) && z) return  2;
	if (x && (!y) && (!z)) return  6;
	if ((!x) && y && (!z)) return  4;
	return MS::kFailure;
}


MStatus  splashTriBox::worldToBary(const MPoint &p, MVector &bary) {

	double ax, ay, bx, by, cx, cy, px, py;
	double nx=fabs(_normal.x);
	double ny=fabs(_normal.y);
	double nz=fabs(_normal.z);
	if (nx > ny) { // y is not greatest
		if (ny > nz) { // x is greatest so use y and z
			ax=_v[0].y; ay=_v[0].z; bx=_v[1].y; by=_v[1].z;  cx=_v[2].y; cy=_v[2].z; px=p.y; py=p.z;
		} else { // z is greater than y
			if (nz > nx) { // z is greatest so use x and y
				ax=_v[0].x; ay=_v[0].y; bx=_v[1].x; by=_v[1].y;  cx=_v[2].x; cy=_v[2].y; px=p.x; py=p.y;
			} else { // x is greatest so use y and z
				ax=_v[0].y; ay=_v[0].z; bx=_v[1].y; by=_v[1].z;  cx=_v[2].y; cy=_v[2].z; px=p.y; py=p.z;
			}
		}
	}else {// x is not greatest
		if (ny > nz) { // y is greatest so use x and z
			ax=_v[0].x; ay=_v[0].z; bx=_v[1].x; by=_v[1].z;  cx=_v[2].x; cy=_v[2].z; px=p.x; py=p.z;
		} else {//  y is not greatest
			ax=_v[0].x; ay=_v[0].y; bx=_v[1].x; by=_v[1].y;  cx=_v[2].x; cy=_v[2].y; px=p.x; py=p.y;
		}
	}	
	double b0 =  (bx - ax) * (cy - ay) - (cx - ax) * (by - ay);

	if (fabs(b0) < EPSILON) {
		return MS::kFailure;
	}
	double b0Recip = 1.0 / b0 ;
	bary.x = ((bx - px) * (cy - py) - (cx - px) * (by - py)) * b0Recip 	;
	bary.y = ((cx - px) * (ay - py) - (ax - px) * (cy - py)) * b0Recip	;
	bary.z = ((ax - px) * (by - py) - (bx - px) * (ay - py)) * b0Recip	;
	return MS::kSuccess;
	
}

MPoint splashTriBox::baryToWorld(const MVector &bary) 
{
	return  MPoint(
		(_v[0].x*bary.x+_v[1].x*bary.y+_v[2].x*bary.z) ,
		(_v[0].y*bary.x+_v[1].y*bary.y+_v[2].y*bary.z) ,
		(_v[0].z*bary.x+_v[1].z*bary.y+_v[2].z*bary.z)
	);
}

bool splashTriBox::baryIntersections(
	const MVector &bary1,
	const MVector &bary2,
	MVectorArray &resBarys
	)
{
	// if the two sets of baryCoords passed in to the function are inside the triangle then return them
	// if one is outside then return the other one and the intersection with the edge
	// if both are outside then return all valid edge intersections (there will be zero or two of these)
	resBarys.clear();


	int r1 = getRegion( bary1);
	int r2 = getRegion( bary2);
	// test the regions 
	// return false if both points are in the same set of adjacent regions - i.e.
	// both in (2-3-4) or  both in (4-5-6) or  both in (6-1-2) 
	if ( (r1>1 && r1<5) && (r2>1 && r2<5) ){
		//cerr << "EAST" <<endl;
		return false;
	}
	if ( (r1>3 && r1<7) && (r2>3 && r2<7) ){
		//cerr << "WEST" <<endl;
		return false;
	}
	if ( (r1==1 || r1==2 || r1==6) && (r2==1 || r2==2 || r2==6)){
		//cerr << "SOUTH" <<endl;
		return false;
	}
	//cerr <<" passed=========="<<endl;

	MVector baryDiff = (bary2 - bary1);
	MVector baryNew;
	double fraction ;
	if (!r1) resBarys.append(bary1);
	if (!r2) resBarys.append(bary2);
	if (resBarys.length() == 2) return true;
	//cerr << "here 1"<<endl;
	if (sign(bary1.x) != sign(bary2.x)) { // test YZ
		fraction = -(bary2.x / baryDiff.x);
		baryNew = (fraction*baryDiff) + bary2;
		if ( inRange(baryNew.y)  &&  inRange(baryNew.z) ){
			resBarys.append(baryNew);
			if (resBarys.length() == 2) return true;
		}
	}
	
	//cerr  <<"here 2"<<endl;
	if (sign(bary1.y) != sign(bary2.y)) { // test XZ
		fraction = -(bary2.y / baryDiff.y);
		baryNew = (fraction*baryDiff) + bary2;
		if ( inRange(baryNew.x)  &&  inRange(baryNew.z) ){
			resBarys.append(baryNew);
			if (resBarys.length() == 2) return true;
		}
	}
	//cerr  <<"here 3"<<endl;
	
	if (sign(bary1.z) != sign(bary2.z)) { // test XY
		fraction = -(bary2.z / baryDiff.z);
		baryNew = (fraction*baryDiff) + bary2;
		if ( inRange(baryNew.x)  &&  inRange(baryNew.y) ){
			resBarys.append(baryNew);
			if (resBarys.length() == 2) return true;
		}
	}
	//cerr  <<"here 4"<<endl;
	//cerr << "out baryIntersections"<<endl;
	return false;
}

bool splashTriBox::trianglesIntersect(splashTriBox  *other, MPoint &start, MPoint &end){
	// cerr << "in trianglesIntersect"<<endl;
	// find out which side of our plane the other vertices lie on


	MVector p0 = other->vertex(0) - _v[0];

	MVector p1 = other->vertex(1) - _v[0];

	MVector p2 = other->vertex(2) - _v[0];

	double dot0 =  p0*_normal; 
	double dot1 =  p1*_normal;
	double dot2 =  p2*_normal;
	int sign0 = sign(dot0);
	int sign1 = sign(dot1);
	int sign2 = sign(dot2);
	// cerr << "got signs"<<endl;
	// if they all lie on the same side of the plane (or 2 of them 
	// do and the other is on the plane) then there is no intersection
	
	if (abs(sign0+sign1+sign2) >= 2) {
		return false;
	}
	// if two or more lie on the plane then there is no intersection
	if ( ((sign0 == 0) + (sign1 == 0) + (sign2 == 0)) >=  2 ) {
		return false;
	}
	// now we have two verts on one side of the plane and one on the other,
	// so get the intersections with the plane
	//cerr << "sign mask "<<sign0<<sign1<<sign2<<endl;
	MPointArray res(2,MPoint::origin);
	//cerr << "made res(2)"<<endl;
	int index = 0;
	
	// get the intersection points with the plane
	///////////////////////////////////////////////////
	
	bool crossesPlane;
	if (sign0 != sign1)  {
		//cerr << "(sign0 != sign1)" <<endl;
		crossesPlane = lineIntersectsPlane(_normal, _v[0], other->vertex(0), other->vertex(1), res[index]);
		if (crossesPlane) index++;
	}
	
	
	if (sign0 !=sign2)  {
		//cerr << "(sign0 != sign2)" <<endl;
		crossesPlane = lineIntersectsPlane(_normal, _v[0], other->vertex(0), other->vertex(2), res[index]);
		if (crossesPlane) index++;
	}
	
	if (index < 2) {
		if (sign1 != sign2) {
			//cerr << "(sign1 != sign2)" <<endl;
			crossesPlane = lineIntersectsPlane(_normal, _v[0], other->vertex(1), other->vertex(2), res[index]);
			if (crossesPlane) index++;
		}
	}
	if (index != 2) {
		// cerr << "something very strange happened"<<endl;
		return false; 
	}
	//makeLocator(res[0]);
	//makeLocator(res[1]);
	
	//cerr << res << endl;
	// res(2) should now contain both intersections so next we project 
	// everything into 2d and calculate barycentrics
	///////////////////////////////////////////////////
	//int r1,r2;
	MVector bary1,bary2;
	//double u1,v1,w1,u2,v2,w2 ;
	// cerr << "about to worldToBary 1"<<endl;
	worldToBary(res[0],bary1);
	// cerr << "about to worldToBary 2"<<endl;
	worldToBary(res[1],bary2);
	MVectorArray resBarys;
	
	// cerr << "about to baryIntersections"<<endl;
	bool intersected = baryIntersections(bary1,bary2,resBarys);
	
	if (!intersected) {
		return false;
		// cerr << "no intersection 3"<< endl;
	}
	start = baryToWorld(resBarys[0]);
	end = baryToWorld(resBarys[1]);
	// makeLineSegment(start,end);
	// cerr << "out trianglesIntersect"<<endl;
	return true;
}

bool splashTriBox::intersectionDirection(splashTriBox  *other,MVector &result){
	// 
	const MVector &n1 = _normal;
	const MVector &n2 = other->normal();
	
	MVector dir = n1^n2;		
	double dirlen = dir.length();	
	if (dirlen < EPSILON) return false;
	
	double d1_0 = MVector(_v[0])*n1;
	double d2_0 = MVector(other->vertex(0)) * n2;

	double d1_1 = MVector((_v[0]+_change))*n1;
	double d2_1 = MVector(other->vertex(0)+other->change()) * n2;
	
	//cerr << "_change " << _change << " other->change() "<< other->change() << endl;
	
	//dir = dir / dirlen;
	
	double abs;
	double maxabs = fabs(dir.x);
	int index = 0;
	// find the largest axis
  	if ((abs = fabs(dir.y)) > maxabs) { maxabs = abs ; index = 1; }
  	if ((abs = fabs(dir.z)) > maxabs) { maxabs = abs ; index = 2; }
	MVector p0, p1;
	switch ( index )
	{
		case 0:
		p0 = MVector(	0.0,
			(n1.z * d2_0 - n2.z * d1_0) / dir.x,
			(n2.y * d1_0 - n1.y * d2_0) / dir.x 
		);
		p1 = MVector(	0.0,
			(n1.z * d2_1 - n2.z * d1_1) / dir.x,
			(n2.y * d1_1 - n1.y * d2_1) / dir.x 
		);
		break;
		case 1:
		p0 = MVector((n2.z * d1_0 - n1.z * d2_0) / dir.y,
			0.0,
			(n1.x * d2_0 - n2.x * d1_0) / dir.y 
		);
		p1 = MVector((n2.z * d1_1 - n1.z * d2_1) / dir.y,
			0.0,
			(n1.x * d2_1 - n2.x * d1_1) / dir.y 
		);
		break;
		case 2:
		p0 = MVector( (n1.y * d2_0 - n2.y * d1_0) / dir.z,
             (n2.x * d1_0 - n1.x * d2_0) / dir.z,
             0.0 
		);
		p1 = MVector( (n1.y * d2_1 - n2.y * d1_1) / dir.z,
             (n2.x * d1_1 - n1.x * d2_1) / dir.z,
             0.0
		);
		break;
		default: return false ;  /* Impossible */
	}
	//cerr <<" p0 "<< p0 << " p1 "<< p1 << endl;;
	MVector diff = p1 - p0;
	if ( diff.length() < EPSILON) return false;
	MVector n3 = diff^dir;
	result = (n3^dir).normal();
	result *= double(diff*(-result));
	return true;
	
}
