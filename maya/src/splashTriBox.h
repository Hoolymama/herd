#ifndef splashTriBox_H
#define splashTriBox_H

#include <maya/MIOStream.h>
#include <math.h>
//#include "tritri.h"

// #include <maya/MVector.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MBoundingBox.h>
#include <maya/MPointArray.h>
#include <maya/MVectorArray.h>
#include <maya/MVector.h>
#include <vector>

#include "errorMacros.h"
#include "mayaMath.h"

//enum axis {xAxis, yAxis ,zAxis };

typedef mayaMath::axis axis;

class splashTriBox;

using namespace std;
typedef vector<splashTriBox> VECTOR_OF_TRIBOXES;
typedef vector<splashTriBox*> VECTOR_OF_TRIBOX_POINTERS;


class splashTriBox{
	public:
		splashTriBox();
		splashTriBox( MDynSweptTriangle &t, unsigned el);
		~splashTriBox();
		
		splashTriBox& operator=(const splashTriBox& other) ;

		const MPoint & center() const;
		const double center(axis a) const ;
		
		
		const double min(axis a) const ;
		const double max(axis a) const ;
		
		bool boxIntersects(const splashTriBox *other);
		bool trianglesIntersect(splashTriBox *other, MPoint &start, MPoint &end);
		bool intersectionDirection(splashTriBox  *other,MVector &result);
		const  MBoundingBox & box() const ;
		const MPoint & vertex(int i) const ;
		const MVector & normal() const;
		const MVector & change() const;
		

		
		void makeBox();
		void makeLocator(const MPoint &p);
		void makeLocator(const MVector &p);
		void makeLineSegment(const MPoint &start, const MPoint &end);
		void registerVisit( splashTriBox *other);
		bool met(const splashTriBox *other);
		bool isMoving();
		const unsigned & id() const;
	private:
		bool baryIntersections(
			const MVector &bary1,
			const MVector &bary2,
			MVectorArray &resBarys
		);
		
		int  getRegion(const MVector &bary);		

		MStatus  worldToBary(
			const MPoint &p,
			MVector &bary
		);
		
		bool lineIntersectsPlane(
			const MVector &n,
			const MPoint &p,
			const MPoint &a,
			const MPoint &b, 
			MPoint &r
		);
		
		MPoint baryToWorld(const MVector &bary) ;
		short sign(double v);
		bool inRange(const double& v) ;
		void computeBoundingBox();
		
		
		MPointArray _v; 
		unsigned m_id;
		MVector _change;
		MVector _normal;
		MPoint _center;
		MBoundingBox  _box;	
		VECTOR_OF_TRIBOX_POINTERS _visited;
		
};

#endif

