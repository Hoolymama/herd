
#include <maya/MIOStream.h>
#include <maya/MTime.h>
#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MPxEmitterNode.h>
#include <maya/MFloatVectorArray.h>

#include "lookup.h"
#include "errorMacros.h"


class ppEmitter: public MPxEmitterNode
{
public:
	ppEmitter();
	virtual ~ppEmitter();
	
	static void		*creator();
	static MStatus	initialize();
	virtual MStatus	compute( const MPlug& plug, MDataBlock& block );
	

	static MObject aPoints;
	static MObject aVelocities;
	static MObject aEmissionVectors;

	static MObject aEmissionShape;
	// static MObject aEmissionShapePP;
	static MObject aEmissionShapeAxis;
	static MObject aEmissionShapeAxisPP;


	static MObject aEmissionRadius;
	static MObject aEmissionRadii;

	static MObject aEmissionRadiusBias;
	static MObject aEmissionRadiusBiasPP;

	static MObject aParentIds;
	static MObject aColors;
	static MObject aRadius;
	static MObject aRadii;
	static MObject aRadiusVariation;
	static MObject aNormalSpeeds;
	static MObject aVectorSpeed;

	static MObject aSpreads;
	static MObject aSpeedRandoms;

	static MObject aSweepShutterOpen;

	static MObject aColorVariation;

	static MTypeId	id;
	

private:
	enum EmissionShape { kSphere, kCircle};
			
// unsigned foo();
unsigned emitSphereFromPoint(
	double dt,
	double sweepOpen,
	double rate,
	const MVector &point,
	const MVector &velocity,
	const MVector &emissionVector,
	double emissionRadius,
	double sphBiasVal,
	double parentId ,
	const MVector &color ,
	double radius,
	double normalSpeed,
	double spread,
	double speedRandom,
	double speed,
	const MVector & colorVariation,
	double radiusVariation,
	MVectorArray &outPos,
	MVectorArray &outVel,
	MDoubleArray &outTime,
	MDoubleArray &outParentRadius,
	MDoubleArray &outParentParticleId,
	MVectorArray &outParentColor
) ;
unsigned emitCircleFromPoint(
	double dt,
	double sweepOpen,
	double rate,
	const MVector &point,
	const MVector &velocity,
	const MVector &emissionVector,
	const MVector & emissionShapeAxis,
	double emissionRadius,
	double sphBiasVal,
	double parentId ,
	const MVector &color ,
	double radius,
	double normalSpeed,
	double spread,
	double speedRandom,
	double speed,
	const MVector & colorVariation,
	double radiusVariation,
	MVectorArray &outPos,
	MVectorArray &outVel,
	MDoubleArray &outTime,
	MDoubleArray &outParentRadius,
	MDoubleArray &outParentParticleId,
	MVectorArray &outParentColor
) ;

	double	doubleValue( MDataBlock& block, MObject &att );
	MVector	vectorValue( MDataBlock& block, MObject &att );
	bool	isFullValue( int plugIndex, MDataBlock& block );
	long	seedValue( int plugIndex, MDataBlock& block );
	double	inheritFactorValue( int plugIndex, MDataBlock& block );

	MTime	currentTimeValue( MDataBlock& block );
	MTime	startTimeValue( int plugIndex, MDataBlock& block );
	MTime	deltaTimeValue( int plugIndex, MDataBlock& block );

	// for calculating triangle acceleration
	// MVectorArray m_lastVelocity;
	// values to interpolate
	// MDoubleArray m_lastEmissionRate;
	// MVectorArray m_lastEmissionVec;	
	// unsigned m_lastTriangleCount;
};

// inlines
//
inline double ppEmitter::doubleValue( MDataBlock& block , MObject &att)
{
	MStatus status;

	MDataHandle hValue = block.inputValue( att, &status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


inline MVector ppEmitter::vectorValue( MDataBlock& block,MObject &att )
{
	MStatus status;
	MVector valueV(0.0, 0.0, 0.0);

	MDataHandle hValue = block.inputValue( att, &status );

	if( status == MS::kSuccess )
	{
		double3 &value = hValue.asDouble3();

		valueV[0] = value[0];
		valueV[1] = value[1];
		valueV[2] = value[2];
	}

	return(valueV );
}

inline bool ppEmitter::isFullValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	bool value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( mIsFull, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asBool();
		}
	}

	return( value );
}
inline long ppEmitter::seedValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	long value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( mSeed, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asInt();
				//cerr << value << endl;
		}
	}

	return( value );
}

inline double ppEmitter::inheritFactorValue(int plugIndex,MDataBlock& block)
{
	MStatus status;
	double value = 0.0;

	MArrayDataHandle mhValue = block.inputArrayValue( mInheritFactor, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asDouble();
		}
	}

	return( value );
}

inline MTime ppEmitter::currentTimeValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mCurrentTime, &status );

	MTime value(0.0);
	if( status == MS::kSuccess )
		value = hValue.asTime();

	return( value );
}

inline MTime ppEmitter::startTimeValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = block.inputArrayValue( mStartTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}

	return( value );
}

inline MTime ppEmitter::deltaTimeValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = block.inputArrayValue( mDeltaTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}
	return( value );
}

